mon_dictionnaire = {}
mon_dictionnaire[0] = "a"
mon_dictionnaire[1] = "e"
mon_dictionnaire[2] = "i"
mon_dictionnaire[3] = "o"
mon_dictionnaire[4] = "u"
mon_dictionnaire[5] = "y"
mon_dictionnaire
{0: 'a', 1: 'e', 2: 'i', 3: 'o', 4: 'u', 5: 'y'}
Exemple de suppression
del mon_dictionnaire[0]
>>> mon_dictionnaire
{1: 'e', 2: 'i', 3: 'o', 4: 'u', 5: 'y'}

echiquier = {}
echiquier('a', 1) = "tour blanche" # En bas à gauche de l'échiquier
echiquier('b', 1) = "cavalier blanc" # À droite de la tour
echiquier('c', 1) = "fou blanc" # À droite du cavalier
echiquier('d', 1) = "reine blanche" # À droite du fou
# ... Première ligne des blancs
echiquier('a', 2) = "pion blanc" # Devant la tour
echiquier('b', 2) = "pion blanc" # Devant le cavalier, à droite du pion
# ... Seconde ligne des blancs
{('a', 1): 'tour blanche', ('b', 1): 'cavalier blanc', ('c', 1): 'fou blanc', ('d', 1): 'reine blanche', ('a', 2): 'pion blanc', ('b', 2): 'pion blanc'}