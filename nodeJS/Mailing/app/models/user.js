var bcrypt = require('bcrypt-nodejs');
const db = require ('../models');

module.exports = function(sequelize, Sequelize) {
 
   var user = sequelize.define('user', {
	
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
	 
		nom : {
			type: Sequelize.STRING,
			allowNull: false,
			validate : {
				is: {
					args : ["^[a-z]+$",'i'],
					msg : "uniquement des lettres"
				}
			}
		},
		prenom : {
			type: Sequelize.STRING,
			allowNull: false,
			validate : {
				is: {
					args : ["^[a-z]+$",'i'],
					msg : "uniquement des lettres"
				}
			}
		},
		email : {
			type : Sequelize.STRING,
			allowNull: false,
			unique : true,
			isEmail: true
		},
		password : {
			type : Sequelize.STRING,
			allowNull: false
		}		
	},
	{
		timestamps : false,
		freezeTableName: true,
		instanceMethods: {
			generateHash: function (password, done) {
				bcrypt.genSalt(process.env.SALT_WORK_FACTOR, function (err, salt) {
					bcrypt.hash(password, salt, null, done);
				});
			}
		},
		classMethods: {
			associate: function(models) {
				user.hasMany(models.campagne);
				user.belongsTo(models.entreprise, {targetKey: 'id', foreignKeyConstraint:false});
			}
		}

	})

    user.beforeCreate(function (user, options, done) {
        user.generateHash(user.password, function (err, encrypted) {
            if (err) return done(err);
            user.password = encrypted;
            done(null, user);
        })
    })

    console.log('models/user');
	return user;

} //fin de la variable user


