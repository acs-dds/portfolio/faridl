<div class="col-xs-12 achievo-list">
	<?php foreach($succes as $s): ?>
			<?php //var_dump($s); ?>
        <!-- Page Features -->
            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://faridl.dijon.codeur.online/achievo/css/images/800.500/<?php echo $s->id; ?>.png" alt="">
                    <div class="caption">
                    <ul>
                        <li> <h3><?= $s->titre ?></h3></li>
                        <li><h4><?= $s->intitule ?></h4></li>
                        <li><h6>Votre progression</h6></li>
                        <li>
                            <div class="progress2">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $s->progression; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $s->progression/$s->objectif *100; ?>%">
                            </div>
                          </div>
                        </li>
                        <li><p><strong>objectif :</strong> vous devez atteindre la valeur de <?= $s->objectif ?>%</p></li>
                        <li><a href="<?php echo site_url('achievo/view/'.$s->id); ?>" class="btn btn-primary">View!</a></li>
                    </ul>                         
                    </div><!-- fin de caption  -->
                </div><!-- fin de thumbnail  -->
            </div><!-- fin de col-md-3 col-sm-6 hero-feature  -->
            
	<?php endforeach; ?>
</div>