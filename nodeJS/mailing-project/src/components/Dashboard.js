import React from 'react';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TopBar from './common/TopBar';
import Campagne from './Dashboard/Campagne';
import User from './Dashboard/User';


export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			display: ""
		}

		this.displayUser = this.displayUser.bind(this);
		this.displayCampagne = this.displayCampagne.bind(this);
	}

	displayUser () {
		this.setState({display: <User />})
	}

	displayCampagne () {
		this.setState({display: <Campagne />})
	}

	render() {
		return (
			<div className="dashboard">
				<TopBar />
				<div className="dashboard-content">
					<Paper>
						<Menu>
							<MenuItem primaryText="Utilisateur" onClick={this.displayUser} />
							<MenuItem primaryText="Campagne" onClick={this.displayCampagne}  />
							<MenuItem primaryText="Logout" href='http://faridl.dijon.codeur.online:8080/logout' />
						</Menu>
					</Paper>
					<section>
						{this.state.display}
					</section>
				</div>
			</div>
		)
	}
}