const db = require('../app/models');
const acl = require('../config/acl');

function getUserIdByEmail(email) {
	return db.user.findOne({
		where: {email: email}
	})
	.then(res => {
		return res.id;
	});
}

exports.create = function (req, res, err) {

	if(!req.body){
		const message = "un problème s'est produit avec le formulaire";
	}

	const nom = req.body.nom;
	const prenom = req.body.prenom;
	const email = req.body.email;
	const entreprise = req.body.entreprise;
	const password =req.body.password;
	const confirmPassword = req.body.password2;
	const role = req.body.role;

	req.assert('email', 'Email au mauvais format').isEmail();
	req.assert('password', 'Veuillez insérer un mdp').notEmpty();
	req.sanitize('email').normalizeEmail({ remove_dots: false });
	req.assert('email', 'Votre adresse email n\'est pas valide').isEmail();
	req.assert('password', 'le mdp est trop court').len(3);
	req.assert('confirmPassword', 'les mdp sont différents').equals(req.body.password);

	const validationErrors = req.validationErrors();

	if (validationErrors) {
		//console.log(validationErrors);
		res.render('http://faridl.dijon.codeur.online:3000/dashboard', {
			errors : validationErrors
		});
	}

	return db.user.create({
		nom : nom,
		prenom : prenom,
		entreprise: entreprise,
		email : email,
		password : password
	})
	.then(function(user){
		getUserIdByEmail(user.email).done(user_id => {
			acl.addUserRoles(user_id,role);
		res.send('done');
		})
		//User's Roles
		
	})
	.catch(function(err){
        console.log(err);
		res.render('http://faridl.dijon.codeur.online:3000/dashboard', {
			messages : err.errors
		});
	});
}

exports.getUserId = function(req) {
	if(req && req.user && req.user.id) {
		return db.user.findOne({
			where: {id: req.user.id}
		})
		.then(res => {
			return res ? res.id : null;
		});
	}	else {
		return null;
	}
}