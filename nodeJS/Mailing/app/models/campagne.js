var db = require('../models');
var user = require('./user');

module.exports = function(sequelize, Sequelize) {
 
   var campagne = sequelize.define('campagne', {
	
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
		nom_campagne : {
			type: Sequelize.STRING,
			allowNull: false,
			unique : {
				args : true,
				msg : 'Le nom de campagne doit être unique'
			}
		},
		debut_campagne : {
			type: Sequelize.DATE
		},
		fin_campagne : {
			type : Sequelize.DATE
		},
		gabarit : {
			type : Sequelize.STRING
		},
		quota : {
			type : Sequelize.INTEGER
		},
		userId : { 
			type: Sequelize.INTEGER, 
			references: {
				modele : 'user',
				key : 'id'
			}
		}
	},
	{
		timestamps : false,
		freezeTableName: true,
		classMethods: {
			associate: function(models) {
			// Adds FKey to Campagne and take the primary key from the table user as foreignkey
				campagne.belongsTo(models.user, {targetKey: 'id', foreignKeyConstraint:true});
			}
		}
	});
   	console.log('models/campagne');
	return campagne;

} //fin de la variable user





