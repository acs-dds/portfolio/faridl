<?php

class Access {
	private $session;

	public function __construct() {
		$CI =& get_instance(); // récupère le controller en cours
		//$CI->load->helper('url'); // autoloadé
		//$CI->load->library('session'); // autoloadé
		$this->session =& $CI->session; // récupère l'objet Session de ce controller
	}

	public function restrict() {
		if ($this->session->has_userdata('utilisateur')) return; // si l'élément 'utilisateur' existe dans le tableau de session, on arrête la méthode et le reste du code de l'action dans le contrôleur s'exécutera normalement
		redirect('signin'); // sinon, on redirige vers l'action qui sert à se logger
	}
}