<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChatController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	/*public function index()
	{
		$this->load->view('welcome_message');
	}*/


	private $mapper;

	public function __construct ($discussion) {
		setlocale(LC_TIME, 'fr_FR');
		$this->mapper = new MessageMapper($discussion);
	}

	public function getMessage_CI ($d){
		return $this->mapper->getMessage($d);
	}

	public function addMessages_CI($a, $d, $c){
		return $this->mapper->addMessages($a, $d, $c);
	}

	public function view($page = 'home')
	{

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
	}//fin de view


}//fin ChatController

}
