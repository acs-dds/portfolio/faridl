import React from 'react';
import axios from 'axios';

export default class DeleteCampagne extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nom_campagne : "",
      messages : []
    }
  }

  handleChange (e) {
      this.setState({nom_campagne: e.target.value});
  }

  delete () {

    axios({
      method: 'delete', 
      url: 'http://faridl.dijon.codeur.online:8080/campagne',
      credentials : 'include',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      params: {
        nom_campagne : this.state.nom_campagne
      }
    })
    .then(res => {
      console.log('res',res);
      console.log('res.data.messages',res.data.messages);
      this.setState({
        messages : res.data.messages
      });
    })
    .catch(function (err) {
      console.log(err);
    });
  }

  render() {

      return (
        <div>
          <div>
            {this.state.messages.map(function(message){
            if (message){
              return (
                <div key={message.message}>
                  <p>{message.message}</p>
                  <p>{message.msg}</p>
                </div>
              )
            }
          })}
          </div>

          <form>
            <label htmlFor='nom_campagne'> Nom de la campagne à suppimer </label>
            <input type='text' placeholder='nom de la campagne' name='nom_campagne' value={this.state.nom_campagne} onChange={this.handleChange.bind(this)} />
            <br/>

            <button type='button' onClick={this.delete.bind(this)} > Supprimer une campagne </button>
          </form>
        </div>
      )
  }
}