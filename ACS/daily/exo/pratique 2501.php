<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<?php 

function encode($character) {
	$c = ord($character);
	//la fonction ord() donne la valeur de la table ASCII de la 1ère lettre seulement
	//par exemple h, on a $c = 104 
	if (!($c % 3)) {
	// si 104 modulo 3 n'existe pas alors 
		return chr($c * 2 + 15); //on retourne le charactère de la valeur de la table ASCII, ici on retourne la charcatère de la valeur 223 =(104*2 +15). 
	} else return chr($c); //sinon on retourne le charactère de la valeur 104 soit h
}

$test = "è";
var_dump(encode($test));
// il ressort uniquement la 1ere lettre ou chiffres mais pas les charactères spéciaux "à é etc" il redonne un "?" 
?>
</body>
</html>