<!DOCTYPE html>
<html>
<head>
	<title>Lighton</title>

	<base href='http://faridl.dijon.codeur.online/Lighton/Lighton/CodeIgniter-3.1.3/index.php/'>
	<script src="../jquery-3.1.1.min.js"></script>
</head>
<body>

<?php 
$date=date_create();
$date = date_timestamp_set($date,microtime(true));
?>

<h4>Bravo <strong><?= ucfirst($this->session->userdata('pseudo')); ?> ! </strong> Tu es connecté(e)<br><br> </h4>

<section style="border: 4px solid black;">
	<p>Les personnes connectées sur Lighton</p>
	<div id="users-list"></div>
</section>

<section style="border: 4px solid black;">
	<!-- on fait afficher les messages  -->
	<p>Choisissez votre discussion pour afficher les messages</p>
	<form id="donnees" method="post">
		<input type="hidden" id="login" name="login" value="<?= $this->session->userdata('pseudo'); ?>">
	   <p>
	       <label for="discussion">Dans quelle discussion ?</label><br />
	       <select id="discussion" name="discussion">
	           <option value="general">general</option>
	           <option value="heckton">heckton</option>
	           <option value="newd">newd</option>
	           <option value="salut">salut</option>
	       </select>
	   </p>
	   <button type="button" id="messages-list"> Envoyer</button><br><br>
	</form>
	<div id="affichage"></div>
</section>

<section style="border: 4px solid black;">
	<p>Envoyer un message sur une discussion </p>
	<form method="post" id="envoi">
	  	<label for="message">message</label> : <input type="text" id="message" name="message">
	  	<input type="hidden" name="pseudo" id="pseudo" value="<?= $this->session->userdata('pseudo'); ?>">
		<input type="hidden" id="date" name="date" value="<?php echo date_format($date,"Y-m-d H:i:s"); ?>"><br><br>
		<label for="discussion">discussion</label> : <input type="text" id="discussion" name="discussion" placeholder="general"><br><br>
		<input type="submit" id="send" name="envoyer">
	</form>
	<br />
</section>

<section style="border: 4px solid black;">
	<form action="LoginController/deconnexion">
		<p>A bientôt</p>
		<input type="submit" name="deconnexion" value="Deconnexion">
	</form>
</section>

<script>

$(document).ready(function(){
      
   $("#messages-list").click(function(){
    
   		function showMessages () {
   			$.ajax({
		       url : 'http://faridl.dijon.codeur.online/Lighton/Lighton/CodeIgniter-3.1.3/index.php/ChatController/getMessages',
		       type : 'POST', // Le type de la requête HTTP, ici devenu POST
		       data : $("#donnees").serialize(), // "login=farid&discussion=general"
		       success : function(data){
		       	$("#affichage").html(data);
		       } //fin de success
		    });//fin de $.ajax
		}//fin de showMessages
	    showMessages();
	    return false;

	   
	});//fin de $("#affichage").submit(function(e)

   	$("#envoi").submit(function() {

		function insertMessage(){
			if(pseudo != "" && message != ""){
				$.ajax({
			       url : 'http://faridl.dijon.codeur.online/Lighton/Lighton/CodeIgniter-3.1.3/index.php/ChatController/insertMessage',
			       type : 'POST', // Le type de la requête HTTP, ici devenu POST
			       data : $("#envoi").serialize(),
		    	});//fin de $.ajax
			}//fin if champs vides
        }// fin de function insertMessage(){
        insertMessage();

        function showNewMessages(){
	    	$.ajax({
		       url : 'http://faridl.dijon.codeur.online/Lighton/Lighton/CodeIgniter-3.1.3/index.php/ChatController/getNewMessages',
		       type : 'POST', // Le type de la requête HTTP, ici devenu POST
		       data : $("#donnees").serialize(), // "login=farid&discussion=general"
		       success : function(data){
		       	$("#affichage").appendTo(data);
		       } //fin de success
	    	});//fin de $.ajax
		}//fin de affiche conversation
		showNewMessages();
	    setInterval(showNewMessages, 2000);
	    //on lance la fct showNewMessages toutes les 200ms 
	    //annule l'action qui va vers une autre page
	return false;
	});//fin de $("#envoi").click

   		function afficheUsers(){
		    $.ajax({
		       url : 'http://faridl.dijon.codeur.online/Lighton/Lighton/CodeIgniter-3.1.3/index.php/LoginController/getUtilisateursEnLigne',
		       type : 'POST', // Le type de la requête HTTP, ici devenu POST
		       data : $("#users-list").val(),
		       success : function(data){
		       	$("#users-list").html(data);
		       } //fin de success
		    });
		    return false;//fin de $.ajax
		}//fin de affiche conversation
		afficheUsers(); // on lance la fonction
		setInterval(afficheUsers, 1000); //on relance la fct afficheUsers toutes les 200ms

});//fin de $(document).ready

</script>

</body>
</html>