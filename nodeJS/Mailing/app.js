//library
require('dotenv').config({path:'./.env.sample'});
const pg = require('pg'); 
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const jwt = require('jsonwebtoken');
const flash = require('connect-flash');
//const flash = require('express-flash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

require ('./config/passport.js')(passport);

const expressValidator = require('express-validator');
const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine', 'pug');

//using settings
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));


//Express Session
app.use(session({
	cookie: { maxAge: 60000 },
	saveUninitialized: true,
	resave: 'true',
	secret: process.env.SECRET_KEY
}));

//Passport init
app.use(passport.initialize());
app.use(passport.session());


//Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
	var namespace = param.split('.')
	, root    = namespace.shift()
	, formParam = root;

	while(namespace.length) {
	  formParam += '[' + namespace.shift() + ']';
	}
	return {
	  param : formParam,
	  msg   : msg,
	  value : value
	};
  }
}));


SALT_WORK_FACTOR=process.env.SALT;

//Connect Flash
app.use(flash());

// app.use(function(req, res, next) {
//   res.locals.messages = req.flash();
//   next();
// });


// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    //res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    //res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, application/x-www-form-urlencoded, Authorization, X-Requested-With');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware

    next();
});


// Controllers (route handlers).
const connexionController = require('./controllers/connexionController'); //(app);
const tokenController = require ('./controllers/tokenController');
const userController = require ('./controllers/userController');
const entrepriseController = require('./controllers/entrepriseController')
const campagneController = require('./controllers/campagneController')

//Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));


//token
app.get('/token', tokenController.getToken);
app.get('/token/verif', tokenController.verifyToken);

//register + Login
app.get('/register', connexionController.register);
app.get('/login', connexionController.login);
app.get('/logout', connexionController.destroySession);
app.post('/authenticate', passport.authenticate('local', 
														{ successRedirect: 'http://localhost:3000/', 
														  failureRedirect: 'http://localhost:3000/dashboard',
														  failureFlash: true }));

//dashboard
app.get('/', connexionController.IsAuthenticated, connexionController.dashboard);

app.post('/user', userController.create);
app.post('/entreprise', entrepriseController.create);
app.post('/campagne', campagneController.create);


//Models
var models = require("./app/models");
 
//Sync Database
models.sequelize.sync().then(function() {
	console.log('connecté à la BDD');
}).catch(function(err) {
	console.log(err, "un problème avec la connexion pg!");
});


app.listen(process.env.PORT);

module.exports = app;