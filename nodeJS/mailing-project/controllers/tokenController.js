const nJwt = require('njwt');
const secretKey = process.env.JWT_SECRET_KEY;

exports.getToken = function (req) { //With this function we create tokens related to campaigns datas

    const claims = {
        'nom_campagne': req.nom_campagne
    };

	var jwt = nJwt.create(claims, secretKey, 'HS256');

	jwt.setExpiration(new Date().getTime() + (60*60*10000)); // One hour from now *10
    console.log('new Date().getTime() + (60*60*10000)', new Date().getTime() + (60*60*10000));

	var token = jwt.compact();
    console.log('token', token);

	var url = 'http://faridl.dijon.codeur.online:3000/campagne/' + claims.nom_campagne + '/' + token; // the url we will send to access to the application

    return url;

}


exports.verifyToken = function (req, res) { // We return isLoggedIn to src/Login.js

	var token = req.query.token;

	var Jtw = nJwt.verify(token, secretKey);
	
	nJwt.verify(token, secretKey, function(err, Jtw){
  		if(err){
    		var data = {
    			isLoggedIn: false
    		}

    		res.send(JSON.stringify(data));
    		//on redirige l'utilisateur vers une page d'erreur.
  		} else {
    		console.log('on autorise l\'utilisateur à entrer');
    		//on redirige vers l'app web et le gabarit de la campagne
    		var data = {
    			isLoggedIn: true,
    			nom_campagne : Jtw.body.nom_campagne,
                gabarit: {
                    url: 'zombie.jpg'
                }
    		}

    		res.send(JSON.stringify(data));
  		}
	});
}