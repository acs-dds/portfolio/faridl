<?php
	if (count($_POST) == 0) {
		header('Location: accueil.php');
		exit;
	}

	require 'classes/mapper.php';
	session_start();

	var_dump($_SESSION);

	echo '<br/><br/>';

	var_dump($_POST);

	$p = $_SESSION['produit'];

	list($largeur, $hauteur, $epaisseur) = $_SESSION['dimensions'];

	$volumeGache = $volumeTotal = $largeur * $hauteur * $epaisseur;

	foreach ($_POST['formes'] as $forme) {
		$coords = explode(",",substr($forme, 5, -1));
		switch (substr($forme, 1, 4)) {
			case 'rect':
				$volume = $coords[2] * $coords[3] * $epaisseur;
				break;
			case 'circ':
				$volume = $coords[2] * M_PI * $epaisseur;
				break;
		}

		if (substr($forme, 0, 1) == "+") {
			$volumeGache -= $volume;
		} else {
			$volumeGache += $volume;
		}
	}

	$prix = $volumeGache / 1000 * $p->getPrixGache() + ($volumeTotal - $volumeGache) / 1000 * $p->getPrixForme();
	echo $prix;