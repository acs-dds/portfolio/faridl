
import React from 'react';
import PostService from './common/PostService';
import Dashboard from './Dashboard';
import serialize from 'form-serialize';

class Login extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			redirect: false
		}

		this.authenticate = this.authenticate.bind(this);
	}

	authenticate() {
		var formData = document.getElementById('form_login');
		const data = serialize(formData);

		// We call the server to log the user in.

		PostService.sendData('http://faridl.dijon.codeur.online:8080/authenticate', data)
		.then(res => {
			if(res.sessionID) {
				console.log(res.sessionID);
				this.setState({
					redirect: true,
				});
			}
		})
		.catch(err => {
		  console.log(err);
		})
	}

	render() {
		const redirect = this.state.redirect;

		if (redirect) {
			return <Dashboard />;
		}
		return(
			<div className="login">
				<p>Connexion au dashboard</p>
				<form id='form_login'>
					<input type='text' placeholder='email' name='email' />
					<input type='password' name='password' />
					<input type='button' value="Sign in" onClick={this.authenticate} />
				</form>
			</div>
		)
	}
 
}

export default Login;