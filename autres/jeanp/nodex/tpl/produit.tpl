<article class="<?= $class; ?>">
	<h3><?= $this->essence; ?></h3>
	<h5><?= $longueur; ?> mm x <?= $largeur; ?> mm<?php if (!is_null($epaisseur)): ?> x <?= $epaisseur; ?> mm<?php endif; ?></h5>
	<?php if (is_null($epaisseur)): ?>
	<select class="epaisseur">
		<?php foreach ($this->epaisseurs as $ep): ?>
		<option value="<?= $ep; ?>"><?= $ep; ?> mm</option>
		<?php endforeach; ?>
	</select>
	<a class="choisir" href="<?= $href; ?>">
		<button>Découper</button>
	</a>
	<?php endif; ?>
</article>