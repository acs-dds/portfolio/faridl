import Papa from 'papaparse';
import React from 'react';
import TextFieldSimple from './common/TextFieldSimple';
import Gabarit from './common/Gabarit' ;
import Contact from './common/Contact';
import TopBarIcon from './common/TopBarIcon';

class Layout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      msg: "[Votre message personnalisé ...]",
      accepted: [],
      infos: "",
      successMsg: ""
    }

    this.droppedFile = this.droppedFile.bind(this);
    this.sendInfos = this.sendInfos.bind(this);
    this.changeMsg = this.changeMsg.bind(this);
  }

  changeMsg(newMsg) {
    this.setState({msg: newMsg});
  }

  sendInfos() {
    const selectedContent = document.querySelectorAll("input[type='checkbox']:checked");
    
    if(selectedContent.length > 0) {

      const publiList = [];

      // const emailList = [];
      // const firstNameList = [];
      // const lastNameList = [];

      for (let i = 0; i < selectedContent.length + 1 ; i++) {

        if (document.querySelector("input[id='checked"+i+"']:checked")) {
          publiList.push({
            emailAddress: document.getElementById("emailAddress"+i).value,
            firstName: document.getElementById("firstName"+i).value,
            lastName: document.getElementById("lastName"+i).value
          });
        }
      }

      const data = {
        publiList: publiList,
        exp: document.getElementById('exp').value,
        obj: document.getElementById('obj').value,
        msg: document.getElementById('msg').value,
        camp: this.props.campagne,
        img: this.props.gabarit.url
      }

      fetch('http://faridl.dijon.codeur.online:8080/mail', {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          //'Origin': 'http://nabil.dev.jpm-next.com/3000'
          'Content-Type': 'application/json'
        }
      })
      .then(res => {
        return res.json();
      })
      .then(res => {
        this.setState({successMsg: res.message});
      })
      .catch(err => {
        console.log(err);
      });
    }
  }

  droppedFile(accepted, rejected) {
    this.setState({
      accepted: accepted
    }, () => {
       const email = 'E-mail Address',
        first = 'First Name',
        last = 'Last Name';

      Papa.parse(this.state.accepted[0], {
        encoding: 'ISO-8859-1',
        delimiter: ',',
        header: true,
        skipEmptyLines: true,
        complete: (res) => {
          const data = [];
          for (let i = 0; i < res.data.length; i++) {
            data.push({
              firstName: res.data[i][first],
              lastName: res.data[i][last],
              emailAddress: res.data[i][email]
            });
          }
          this.setState({infos:data});
        }
      });
    });

   
  }

  render() {
    return (
      <div>
        <TopBarIcon title={this.props.campagne} />
        <TextFieldSimple changeMsg={this.changeMsg} />
        <Gabarit message={this.state.msg} campagne={this.props.campagne} gabarit={this.props.gabarit} />
        <Contact droppedFile={this.droppedFile} sendInfos={this.sendInfos} infos={this.state.infos} successMsg={this.state.successMsg} />
      </div>
    );
  }
}

export default Layout;