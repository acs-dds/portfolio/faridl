module.exports = function(sequelize, Sequelize) {
 
   var entreprise = sequelize.define('entreprise', {
	
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},
		nom_entreprise : {
			type: Sequelize.STRING,
			allowNull: false
		}
	},
	{
		timestamps : false,
		freezeTableName: true,
		classMethods: {
			associate: function(models) {
				entreprise.hasMany(models.user);
			}
		}
	})

   console.log('models/entreprise');
	return entreprise;

} //fin de export model
