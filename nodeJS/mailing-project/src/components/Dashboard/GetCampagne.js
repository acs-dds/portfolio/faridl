import React from 'react';
import Moment from 'moment';

export default class GetCampagne extends React.Component {

  constructor() {
    super();

    this.state = {
      checked : "",
    }
  }

  update () {


  }



  render () {

    return (
      <section>
        <table>
        <caption>Liste des campagnes</caption>
          <thead>
           <tr>
               <th>id</th>
               <th>Nom</th>
               <th>Début</th>
               <th>Fin</th>
               <th>gabarit</th>
               <th>quota</th>
               <th>user</th>
           </tr>
          </thead>
          <tbody>
              {this.props.data.map(function(campagne){
                Moment.locale('fr');
                var ddc = campagne.debut_campagne; // ddc = Date Debut de Campagne
                var dfc = campagne.fin_campagne; // ddc = Date Fin de Campagne
                return (
                <tr key={campagne.id}> 
                    <td>{ campagne.id } </td>
                    <td>{ campagne.nom_campagne }</td>
                    <td>{ Moment(ddc).format("LLLL") }</td>
                    <td>{ Moment(dfc).format("LLLL") }</td>
                    <td>{ campagne.gabarit }</td>
                    <td>{ campagne.quota }</td>
                    <td>{ campagne.userId }</td>
                </tr>
                )
              })}
          </tbody>
        </table>
      </section>
    )
  };
}

//<button type='button' onClick={this.update.bind(this)} > mettre à jour une campagne </button>
//<input type="checkbox" checked={this.state.checked} onChange={() => this.setState({checked: !this.state.checked})}/>
