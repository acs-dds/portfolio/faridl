import React from 'react';
import AppBar from 'material-ui/AppBar';

class TopBarIcon extends React.Component {
  
	render() {
		return (
			<AppBar
				showMenuIconButton={false}
				title={this.props.title}
				style={{
					backgroundColor: "#000"
				}}
			/>
		)
  	}
}

export default TopBarIcon;