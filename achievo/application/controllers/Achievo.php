<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievo extends CI_Controller {

	public function index() {
		$this->access->restrict();
		$this->load->model('succes_model');
		$this->succes_model->setIdEntreprise($this->session->utilisateur->id);
		$this->load->view('common/header');
		$this->load->view('index', ['succes' => $this->succes_model->fetchAllSucces()]);
		$this->load->view('common/footer');
	}

	public function view($id) {
		$this->access->restrict();
		$this->load->model('succes_model');
		$this->succes_model->setIdEntreprise($this->session->utilisateur->id);
		$this->load->view('common/header');
		$this->load->view('view', ['succes' => $this->succes_model->fetchSucces($id)]);
		$this->load->view('common/footer');
	}

	public function update($id) {
		$this->access->restrict();
		$this->load->model('succes_model');
		$this->succes_model->setIdEntreprise($this->session->utilisateur->id);
		$this->succes_model->updateSucces($id, $this->input->post('progression'));
		redirect('achievo/view/'.$id);
	}

	public function signin() {
		$data = [];
		if (!is_null($this->input->post('login'))) {
			// envoi du form
			$this->load->model('utilisateur_model');
			$u = $this->utilisateur_model->fetchUtilisateur($this->input->post('login'), $this->input->post('mdp'));
			if (!empty($u)) {
				$this->session->utilisateur = $u;
				redirect('');
			}
			$data['message'] = "Pas de compte pour cet utilisateur et ce mot de passe";
		}

		$this->load->view('common/header');
		$this->load->view('forms/signin', $data);
		$this->load->view('common/footer');
		// affichage du form
	}
}
