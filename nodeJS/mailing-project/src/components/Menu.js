import React from 'react'

import Dashboard from './Dashboard';
import TopBar from './common/TopBar';


class Menu extends React.Component {



	render() {
		return(
			<div>
				<TopBar />
				<Dashboard />
			</div>
		)
	}
 
}

export default Menu;


