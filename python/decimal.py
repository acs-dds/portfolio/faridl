#-*-coding:Latin-1 -*
import os # On importe le module os

def afficher_flottant(flottant):
    """Fonction prenant en paramètre un flottant et renvoyant une chaîne de caractères représentant la troncature de ce nombre. La partie flottante doit avoir une longueur maximum de 3 caractères.

    De plus, on va remplacer le point décimal par la virgule"""
    
    if type(flottant) is not float:
        raise TypeError("Le paramètre attendu doit être un flottant")
    flottant = str(flottant)
    partie_entiere, partie_flottante = flottant.split(".")
    # La partie entière n'est pas à modifier
    # Seule la partie flottante doit être tronquée
    return ",".join([partie_entiere, partie_flottante[:3]])


try:
	flottant_user = input("insérer un chiffre flottant : ") #1.999999999999
	print (flottant_user)
	test = afficher_flottant(flottant_user) #return '1,999'
	print (test)
except:
	print("la fct ne fonctionne pas")

input("insérer un chiffre flottant : ") #1.999999999999


os.system("pause")


