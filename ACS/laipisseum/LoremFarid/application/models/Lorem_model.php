<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Lipsum_model extends CI_Model {

	private $base = ["maecenas", "sit", "amet", "aliquam", "nunc", "ac", "pellentesque", "nisl", "nunc", "sapien", "ligula", "pellentesque", "quis", "cursus", "eget", "finibus", "et", "quam", "morbi", "ornare", "ullamcorper", "convallis", "ut", "at", "leo", "varius", "pretium", "felis", "eget", "tempus", "arcu", "aenean", "aliquet", "lorem", "quis", "auctor", "egestas", "nam", "vitae", "ipsum", "a", "lectus", "pretium", "venenatis", "pulvinar", "eu", "est", "morbi", "ullamcorper", "nibh", "sit", "amet", "enim", "sagittis", "at", "volutpat", "ligula", "placerat", "duis", "sem", "augue", "tempor", "sed", "massa", "cursus", "aliquam", "blandit", "nisi", "sed", "eleifend", "tincidunt", "dui", "quis", "laoreet", "massa", "efficitur", "et", "sed", "dui", "sapien", "volutpat", "et", "finibus", "nec", "ullamcorper", "suscipit", "mauris", "cras", "ultrices", "ex", "et", "nulla", "congue", "tempus", "curabitur", "finibus", "commodo", "tristique", "nullam", "pharetra", "ex", "at", "purus", "sodales", "non", "faucibus", "neque", "scelerisque"];

	private $themes = [
		'zombies' => ['Zombies','Cerveau','Démembrer','Ramper','Tripes','Sang','Gore'],
		'glace' => ['Macadamia Nut Brittle','Vanille Pecan','Pralines & Cream','Cookies & Cream','Strawberry Cheesecake','Chocolate Midnight Cookies'],
		'metal' => ['Leprous','Immortal','Iron Maiden','Slayer','Satan\'s Penguins','Behemoth','Borknagar','Tyr','Architects']
	];


	public function genererParagraphesModel($nbParagraphe = 2, $nbMot = 150) {
		$paragrapheGen = "";
		$longueurLorem = count($this->Lorem);

		
		for ($j=0; $j < $nbParagraphe; $j++) { 
		$phraseGen = "";

		   	for ($i=0; $i < $nbMot ; $i++) { 
			   	$phraseAleatoire = $this->Lorem[rand(0, $longueurLorem -1)];
			    $motAleatoire = $phraseAleatoire[rand(0, count($phraseAleatoire)-1)];
				$phraseGen .= $motAleatoire ." ";
			}//fin boucle du nombre de mot
		$paragrapheGen .= ucfirst(rtrim($phraseGen)).".<br><br>";
		}//fin boucle du nombre de paragraphe
	echo $paragrapheGen;
	} // fin genererLorem

	/*public function genererTheme ($nom, $auteur, $contenuTheme) {
		if(!file_exists($this->fichier = __DIR__.'/../'.$nom.'.csv')){ 
			touch($this->fichier);
			var_dump($this->fichier);
			$f = fopen ($this->fichier,"w");
			fputcsv($f, $contenuTheme[0]);
			fclose($f);
		}*/
		   
	} // fin genererTheme
	//fin de la class
}