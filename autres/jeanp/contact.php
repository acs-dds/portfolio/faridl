<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
</head>
<body>
<?php require 'menu.php'; ?>
<h1>Laissez-nous un message !</h1>
<?php if (isset($_SESSION['erreurs'])): ?>
<ul>
	<?php foreach ($_SESSION['erreurs'] as $erreur): ?>
	<li><?php echo $erreur; ?></li>
	<?php endforeach; unset($_SESSION['erreurs']); ?>
</ul>
<?php endif; ?>
<form method="post" action="dispatch.php">
	<label for="dest">Destinataire :</label>
	<select name="dest" id="dest">
		<option value="jeanp">Jean</option>
		<option value="marie-pierrel">Marie-Pierre</option>
		<option value="quentinp">Quentin</option>
	</select>
	
	<label for="nom">Nom :</label>
	<input type="text" name="nom" id="nom">
	
	<label for="prenom">Prénom :</label>
	<input type="text" name="prenom" id="prenom">
	
	<label for="email">Mail :</label>
	<input type="text" name="email" id="email">
	
	<label for="message">Message :</label>
	<textarea name="message" id="message"></textarea>

	<input type="submit" value="Envoyer">

</form>
</body>
</html>