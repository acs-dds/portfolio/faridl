<?php
	// pour initialiser 
	session_start();
	// on récupère toutes les données qu'on réduit
	$p = array_map('trim', $_POST);
	$valide = true;
	$erreurs = [];

	if (strlen($p['nom']) < 3) {
		$valide = false;
		$erreurs[] = 'Le nom n\'est pas assez long';
	}
	if (strlen($p['prenom']) < 3) {
		$valide = false;
		$erreurs[] = 'Le prénom n\'est pas assez long';
	}
	if (preg_match("#^[a-z0-9_.-]+@[a-z0-9-]{2,}\.[a-z]{2,}$#i", $p['email']) === 0) {
		$valide = false;
		$erreurs[] = "L'adresse mail n'a pas un format valide";
	}
	if (strlen($p['message']) < 20 || strpos($p['message'], ' ') === false) {
		$valide = false;
		$erreurs[] = "Le message n'est pas valide, il doit contenir au moins 20 caractères et être composé de plusieurs mots";
	}

	

	if (!$valide) {
		// On crée un espace qu'on va rappeler sur une autre page = on lui donne comme valeur $erreur
		$_SESSION['erreurs'] = $erreurs;
		//pour pouvoir récupérer toutes les données de l'ancienne version
		$_SESSION['old_post'] = $_POST;
		// si valide = faux alors on se redirige vers contact.php
		header("Location: contact.php");
		exit;
	}

	// redirection vers les portfolios
	// $dests = []; variable créée dans laquelle on donne l'adresse des portofolios
	$dests = [
		// clé => (pour attribuer une valeur) valeur
		'jeanp' => 'http://jeanp.dijon.codeur.online/crc.php',
		'marie-pierrel' => 'http://marie-pierrel.dijon.codeur.online/Comebackkids/',
		'quentinp' => 'http://quentinp.dijon.codeur.online/PHPExo1/simon.php'
	];
	//la clé est la "value" dans les balises HTML.


	//$_POST['dest']= la valeur de dest donc soit jeanp ou marie-pierrel ou quentinp
	$dest = $dests[$_POST['dest']];
	//maintenant $dest = $dests[jeanp] = http://jeanp.dijon.codeur.online/crc.php

	unset($_POST['dest']);
	// Je retire la case "dest" du tableau car on a plus besoin du destinataire.

	$get = [];
	foreach ($_POST as $key => $value) {
		// ici on a les clés valeurs de $_POST, il contient message, nom, prénom mais pas destinataire car on l'a enlevé avec unset.
		$get[] = "$key=".urlencode($value);
		// urlencode transforme les entités en données URL (%20 pour les espaces, &, etc)
	}
	$get = "?".implode('&', $get);
	// impolde est une fonction qui sépare les données d'un tableau.

	header('Location: '.$dest.$get);
?>

