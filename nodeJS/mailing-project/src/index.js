import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Routes from './Routes';

import './data/css/index.css';

// MuiThemeProvider must be provided to render Material UI components
const App = () => (
  <MuiThemeProvider>
    <Routes />
  </MuiThemeProvider>
);

// We render the application in public/index.html in the DOM Element with the attribute id='root'
ReactDOM.render(
<App />,
  document.getElementById('root')
);