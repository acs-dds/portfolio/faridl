const nJwt = require('njwt');
//const secretKey = process.env.JWT_SECRET_KEY;
const secretKey = 'ananas';


const claims = {
	'id_campagne': 1, //comment renseigner cette info ?
	'nom_campagne': 'Bonne année 2018',
	'admin' : true
};

exports.getToken = function (req, res) {

	var jwt = nJwt.create(claims, secretKey, 'HS256');
	console.log(secretKey);
	jwt.setExpiration(new Date().getTime() + (60*60*10000)); // One hour from now *10

	var token = jwt.compact();
	console.log(token);

	var URL = 'http://blabacarte.fr/campagne/' + claims.id_campagne + '/' + token;
	
    return res.json(URL);
}


exports.verifyToken = function (req, res) {

	var token = req.query.token;

	var Jtw = nJwt.verify(token, secretKey);
	
	nJwt.verify(token, secretKey, function(err, Jtw){
  		if(err){
    		var data = {
    			isLoggedIn: false
    		}
    		res.setHeader('Access-Control-Allow-Origin', '*');
    		res.setHeader('Content-type', 'application/json');
    		res.send(JSON.stringify(data));
    		//on redirige l'utilisateur vers une page d'erreur.
  		} else {
    		console.log('on autorise l\'utilisateur à entrer');
    		//on redirige vers l'app web et le gabarit de la campagne
    		var data = {
    			isLoggedIn: true,
    			nom_campagne : Jtw.body.nom_campagne,
                gabarit: {
                    url: 'zombie.jpg'
                },
    			id_campagne : Jtw.body.id_campagne
    		}
    		res.setHeader('Access-Control-Allow-Origin', '*');
    		res.setHeader('Content-type', 'application/json');
    		res.send(JSON.stringify(data));
  		}
	});
}