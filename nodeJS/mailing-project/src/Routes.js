import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Route, browserHistory } from 'react-router';

import Login from './components/Login';
import Main from "./components/Main";
import Site from "./components/Site";

// Constructing routes on the application side
const Routes = () => (
  <BrowserRouter history={ browserHistory } >
  	<div>
  		<Route exact path="/campagne/:id/:token" component={ Main } />
  		<Route exact path="/" component={ Site } />
  		<Route path="/dashboard" component={ Login }/>
	</div>
  </BrowserRouter>
);

export default Routes;