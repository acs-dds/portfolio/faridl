var campagne = require('../app/models').campagne;
const userController = require ('./userController');
const tokenController = require ('./tokenController');

exports.create = function (req, res, err) {
	if(!(
		req
		&& req.body
		&& req.user
		)
	){
		var message;
		if(!req.user){
			return res.json({messages : [{message : "Attention, Vous n'êtes pas connecté"}, {message : "Reconnectez-vous vers la page login"}]}, 403);
		} if (!(req && req.body)) {
			return res.json({messages : [{message : "Paramètres non définis"}]}, 403);
		} else {
			console.log('=====================> LA FCT COMMENCE');
		}
	}

	//Afficher les erreurs de Validation error

	//Afficher les erreurs de sequelize


	//console.log(req.headers.nom_campagne);
	req.assert('nom_campagne', 'Veuillez insérer le nom de la campagne').notEmpty();
	const validationErrors = req.validationErrors();

	if (validationErrors) {
		console.log('validationErrors', validationErrors);
		// validationErrors [ { 
		// 	param: 'nom_campagne',
  //   		msg: 'Veuillez insérer le nom de la campagne',
  //   		value: undefined } ]
		return res.json({messages : validationErrors});
	}

	var params = req.body;
	//console.log(params)
	//console.log(req);

	console.log('req.user.id', req.user.id);
	console.log('je suis dans campagneCOntr create');
	params.userId = req.user.id;
	params.token = tokenController.getToken(params);

	return campagne.create(params)
 	.then(function (params) {
 		const message = [{message : "Votre campagne a bien été enregistrée"}, {message : "merci"}, {message: tokenController.getToken(params)}];
 		return res.json({ 
 			messages: message
 		})
 	}, function (err){
 		return res.json({messages : err.errors});
 	})
}


exports.get = function (req, res, err) {
	//L'utilisateur qui lance la méthode GET, est passé par passport 
	// sa session a été enregistrée
	// il faut la trouver

	userController.getUserId(req).done(user_id => {
		return campagne.findAll({
			where: { 
				userId: user_id // mettre la valeur qui se trouve dans la session
			}
		})
		.then(function (campagne) {
			return res.json(campagne);
		})
		.catch(err => {
			console.log(err); // message d'erreur à afficher ici si le retour de getUserId est null ?
		})
	})
}


exports.delete = function (req, res, err) {

	console.log('dans la fct delete');

	if (!(req)) {
		const message = [{message : "pas de req"}];
		console.log( "pas de req");
 		return res.json({ 
 			messages: message
 		})
	}
	return campagne.destroy({
	where : {
		nom_campagne: req.query.nom_campagne
		}
	})
 	.then(function (params) {
 		const message = [{message : "Votre campagne a bien été supprimée"}, {message : "merci"}];
 		return res.json({ 
 			messages: message
 		})
 	}, function (err){
 		return res.json({messages : err.errors});
 	})
}

exports.update = function (req, res, err) {

	//console.log(req);
	// console.log('req');
	// console.log(req);

	var params = req.body;

// si on a un name on change le name
	if(params){
		campagne.find({ 
			where: { 
				nom_campagne: req.body.find_campagne
			} 
		})
		.then(res.status(200).json({
 			success: "campagne modifiée"
 		}))
		.then(function (campagne) {
			if (campagne) {
				campagne.updateAttributes({
					nom_campagne: params.name,
					debut_campagne: params.nom_campagne,
					fin_campagne: req.body.fin_campagne,
					gabarit: req.body.gabarit,
					quota: req.body.quota
				})
		    }
		})
		.then(res.redirect('http://faridl.dijon.codeur.online:3000/dashboard', 301))
	} else {
		console.log('Il manque des infos');
	}
}

exports.updateQuota = function (newQuota, camp) {

	campagne.find({ 
		where: { 
			nom_campagne: camp
		} 
	})
	.then(function (campagne) {
		console.log(campagne.quota);
		if (campagne) {
			campagne.updateAttributes({
				quota: campagne.quota - newQuota
			})
	    }
	})
}

