import React from 'react';
import FormCreateCampagne from './CreateCampagne';
import FormDeleteCampagne from './DeleteCampagne';
import FormUpdateCampagne from './UpdateCampagne';
import DisplayCampagne from './GetCampagne';


export default class Campagne extends React.Component {
  
  constructor() {
    super();

    this.state = {
      display : "",
    }

    this.GetCampagne = this.GetCampagne.bind(this);
    this.DisplayCreateCampagne = this.DisplayCreateCampagne.bind(this);
    this.DisplayDeleteCampagne = this.DisplayDeleteCampagne.bind(this);
    //this.DisplayUpdateCampagne = this.DisplayUpdateCampagne.bind(this);
    this.GetCampagne = this.GetCampagne.bind(this);
  }

  DisplayCreateCampagne () {
    this.setState({display : <FormCreateCampagne /> })
  }

  DisplayDeleteCampagne () {
    this.setState({display : <FormDeleteCampagne /> })
  }

  DisplayUpdateCampagne () {
    this.setState({display : <FormUpdateCampagne /> })
  }  

  

  GetCampagne () {
    fetch('http://faridl.dijon.codeur.online:8080/campagne', {
      method: 'get',
      credentials: 'include'
    })
    .then(function(res) {
      // console.log(res.json());
      return res.json();
    })
    .then( data => {
      this.setState({ display : <DisplayCampagne data={data} /> })
    })
    .catch(err => {
      console.log(err);
    })
  }

  render() {

    return (
      <div>
        <h1> Dashboard Campagne </h1>
        <button type="button"
              onClick={this.GetCampagne}>
              READ
        </button>
        <button type="button"
              onClick={this.DisplayCreateCampagne}>
              CREATE
        </button>
        <button type="button"
              onClick={this.DisplayDeleteCampagne}>
              DELETE
       </button>
        <button type="button"
                onClick={this.DisplayUpdateCampagne}>
              UPDATE
       </button>
       <div>
         {this.state.display}
       </div>
      </div>
    )
  }
}