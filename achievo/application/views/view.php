<div class="col-xs-12 achievo-list">
	<!-- <article><?php //var_dump($succes); ?></article> -->
	<h1>Trophées</h1>
	<ul class="trophees">
		<li><img src="http://faridl.dijon.codeur.online/achievo/css/images/<?= $succes->id ?>.png"></li>
		<li><h2><?= $succes->titre; ?></h2></li>
		<li><h4><?= $succes->intitule; ?></h4></li>
		<li><h4>Votre objectif est d'atteindre <?= $succes->objectif; ?>%</h4></li>
		<li>
			<div class="progress">
    			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $succes->progression; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $succes->progression; ?>%">
    			</div>
  			</div>
		</li>
		<li>
			<form method="post" action="<?php echo site_url('achievo/update/'.$succes->id); ?>">
				<input type="number" value="<?php echo $succes->progression; ?>" name="progression"/>
				<input type="submit" value="Mettre à jour"/>
			</form>
		</li>
	</ul>
</div>