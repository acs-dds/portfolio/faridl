<?php

require "classes/classeArticle.php";

class Mapper {

	public static function getCatalogue($typo) {
		$cat = [];

		$h = fopen('/home/quentinp/nodexwood/produits.csv', 'r');

		while ($ligne = fgetcsv($h, 0, ';')) {
			$typos = array_map('trim', explode(',', $ligne[5]));
			if (in_array($typo, $typos)) {
				$cat[] = new Article($ligne);
			}
		}
		return $cat;
	}

	public static function getProduit ($ref) {

		$h = fopen('/home/quentinp/nodexwood/produits.csv', 'r');

		while ($ligne = fgetcsv($h, 0, ';')) {
			// si la référence que l'on cherche correspond à la référence de la ligne alors on crée un nouvel objet
			if ($ref == $ligne[0]) {
				return new Article($ligne);

			}
		}
	}
}