//load bcrypt
var bCrypt = require('bcrypt-nodejs');
const passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('../app/models');

var user = require('../app/models').user;

module.exports = function(passport, user) {

//serialize
// saving the user id to the session.
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});
//To get the User object from this instance
	// deserialize user 
	passport.deserializeUser(function(id, done) {
		db.user.findById(id).then(function(user) {
			if (user) {
				//console.log(user);
				//Unhandled rejection TypeError: Cannot read property 'get' of undefined
				done(null, user);
			} else {
				done(user.errors, null);
			}
		});
	});
	

	//LOCAL SIGNIN
	passport.use('local', new LocalStrategy(
		{
			// by default, local strategy uses username and password, we will override with email
			usernameField: 'email',
			passwordField: 'password',
			passReqToCallback: true // allows us to pass back the entire request to the callback
		},

		function(req, email, password, done) {
	
			var isValidPassword = function(password, userpass) {
				return bCrypt.compareSync(password, userpass);
			};
			db.user.findOne({
				where: {
					email: email
				}
			})
			.then(function(user) {
				if (!user) {
					return done(null, false, {
						message: 'Pas le bon email'
					});
				} else if (!isValidPassword(password, user.password)) {
					return done(null, false, {
						message: 'mdp incorrect'
					});
				} else {
					return done(null, user);
				}
			})
			.catch(function(err){
				return done(null, false, {
					messages: err.errors
				});
			});
		}
	))
}