<?php 

/*http://faridl.dijon.codeur.online/ACS/tchat/classes/MessageMapper.php
*///test de getMessage
//$this->fichier doit être égal = "/home/faridl/tchat/discussions/general"
//pour vérifier on peut créer un fichier avec un autre nom que général

class MessageMapper extends CI_Model{

	public function __construct(){

	}

	public function insertMessage_Model($c, $d, $id_discussion, $id_utilisateur){
		$dataMessage = array(
	        'content' => $c,
	        'date' => $d,
	        'id_discussion' => $id_discussion,
	        'id_utilisateur' => $id_utilisateur
		);

		$this->db->insert('message', $dataMessage);
	}//fin fonction insert


	public function addDiscussion ($discussion) {
		$dataDicsussion = array (
				'intitule' => $discussion
			);

		$this->db->insert('discussion', $dataDicsussion);
	}


	public function getMessages_Model ($id_discussion = 1){
		$sql = "SELECT utilisateur.pseudo, utilisateur.email, discussion.intitule, message.content, message.date FROM message
		JOIN utilisateur ON message.id_utilisateur = utilisateur.id
		JOIN discussion ON message.id_discussion = discussion.id
		WHERE message.id_discussion = ? ORDER BY \"date\" ASC";

		$messages = $this->db->query($sql, array($id_discussion));
		return $messages->result_array();
	}//fin de getMessages_Model


	public function getIdDiscussion_Model ($discussion = 'general') {
		$sql = "SELECT id FROM discussion WHERE intitule = ?";

		$id_discussion = $this->db->query($sql, array($discussion));
		return $id_discussion->row()->id;
	}// fin de getIdDiscussion_Model

	public function getIdUtilisateur_Model ($pseudo) {
		$sql = "SELECT id FROM utilisateur WHERE pseudo = ?";

		$id_utilisateur = $this->db->query($sql, array($pseudo));
		return $id_utilisateur->row()->id;
	}// fin de getIdUtilisateur_Model

	public function getNewMessages_model ($discussion = 'general', $date = 0) {
		$sql = "SELECT utilisateur.pseudo, utilisateur.email, discussion.intitule, message.content, message.date 
		FROM message
		JOIN utilisateur ON message.id_utilisateur = utilisateur.id
		JOIN discussion ON message.id_discussion = discussion.id
		WHERE message.id_discussion = ? AND date > ? ORDER BY \"date\" ASC";

		$newMessages = $this->db->query($sql, array($this->getIdDiscussion_Model($discussion), $this->session->derniereDate));
		return $newMessages->result_array();
	}//fin de getNewMessages_model


//fin MessageMapper 
} 