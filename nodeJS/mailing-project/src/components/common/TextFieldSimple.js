import React from 'react';
import TextField from 'material-ui/TextField';

class TextFieldSimple extends React.Component {
  render() {
    return (
      <div className="textfield" >
        <TextField
          id="exp"
          hintText="Expéditeur"
        /><br />
        <br />
        <TextField
          id="obj"
          floatingLabelText="Objet"
        /><br />
        <TextField
          id="msg"
          hintText="Message"
          multiLine={true}
          rows={2}
          rowsMax={4}
          onChange={(e) => this.props.changeMsg(e.target.value)}
        />
      </div>
    )
  }
  
};

export default TextFieldSimple;