<?php 
session_start();
require "MessageMapper.php";


class ChatController{
	private $mapper;

	public function __construct ($discussion) {
		setlocale(LC_TIME, 'fr_FR');
		$this->mapper = new MessageMapper($discussion);
	}

	public function getMessage_CI ($d){
		return $this->mapper->getMessage($d);
	}

	public function addMessages_CI($a, $d, $c){
		return $this->mapper->addMessages($a, $d, $c);
	}


}//fin ChatController


?>