import React from 'react';

class Gabarit extends React.Component {
	render() {

		let newAlt = this.props.gabarit.url.substr(0, this.props.gabarit.url.lastIndexOf('.'))

		return (
			
			<table className="table-gab" cellSpacing="0" cellPadding="0" style={{tableLayout: "fixed"}} >
				<tbody>
					<tr height='50' style={{backgroundColor: "#212121", color: '#ffffff'}}>
						<td>
							<p>{this.props.campagne}</p>
						</td>
					</tr>
					<tr>
						<td>
							 <img src={require('../../data/img/'+this.props.gabarit.url)} alt={newAlt} data-default="placeholder" data-max-width="600" width='600' height='300'/>
						</td>
					</tr>
					<tr height='50' style={{backgroundColor: "#F5F5F5"}}>
						<td>
							<p>Fixed</p>
						</td>
					</tr>
					<tr height='100' style={{backgroundColor: "#F5F5F5"}}>
						<td>
							<p>{this.props.message}</p>
						</td>
					</tr>
					<tr height='50' style={{borderTop: "1px solid",backgroundColor: "#E0E0E0"}}>
						<td>
							<p>Footer</p>
						</td>
					</tr>
				</tbody>
			</table>
		)
	}
};

export default Gabarit;