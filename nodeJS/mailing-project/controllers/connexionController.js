var exports = module.exports = {};

exports.IsAuthenticated = function(req, res, next){
	if(req.isAuthenticated()){
		next();
	} else {
		res.redirect(403,'http://faridl.dijon.codeur.online:3000/dashboard');
	}
}

exports.destroySession = function(req, res, next) {
	req.logOut();
	req.session.destroy();
	res.redirect('http://faridl.dijon.codeur.online:3000/dashboard');
}