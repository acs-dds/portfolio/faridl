# Dans cette partie côté serveur, nous allons détailler les routes et les méthodes que nous appliquons.

## Phase de génération de token :

Une fois que le chef de projet a validé la maquette, le nombre email souhaité et la durée de la campagne; nous générons un token et un lien avec toutes ses informations.
- Route : http://www.blablacarte.fr/getToken
- Méthode :
	* getToken() 
- vue : Pas de vue lors de la création token.


## Phase de connexion :

Identification de l’utilisateur grâce au Json web Token
- Route : http://www.blablacarte.fr/:id_campagne?token=””
- Méthode :
	* verifyToken()
- vue : On retourne le component “campagne” validé par le chef de projet.

L’utilisateur va recevoir un email envoyé par son chef de projet avec un lien :
L’utilisateur clique sur le lien
	* si le lien est valide, alors il entrera dans l’application web
	* si le lien n’est pas valide, on redirigera l'utilisateur vers la page d’erreur.

Utiliser les Json web Tokens (JWT) enregistrés en session dans le lien pour vérifier sa validité.
Avantages : 
	* ce token va ensuite transiter dans chaque requête entre le client et le serveur.
	* protéger contre les attaques de type CSRF


## Phase d’importation des contacts :

- Route : http://www.blablacarte.fr/destinataire/:id_campagne?token=””
- Méthode :
	* getCsv(){} : importation des contacts en csv.
	* formulaire pour saisir les données manuellement.
- vue : On retourne le component “destinataire” 


## Phase erreur :

- Route : http://www.blablacarte.fr/erreur
- Méthode :
	* redirection vers une page d'erreur ou la page d'accueil
- vue : On retourne le component “erreur”
 
 
## Envoi des emails :

- Route : http://www.blablacarte.fr/sendEmail/:id_campagne?token=””
- Méthode :
	* sendEmail(){} : l'envoi des emails
- vue : On retourne le component “success”