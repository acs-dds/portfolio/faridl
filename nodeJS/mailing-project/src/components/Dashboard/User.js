import React from 'react';
import PostService from '../common/PostService';
import serialize from 'form-serialize';

export default class User extends React.Component {

  createUser() {
    var formData = document.getElementById('form_user');
    const data = serialize(formData);

    // We call the server to log the user in.
   PostService.sendData('http://faridl.dijon.codeur.online:8080/user', data)
   .catch(err => {
      console.log(err);
    })
  }
  
  render() {

    return (
      <div>
        <form id='form_user'>
          <label htmlFor='nom'> Nom </label>
          <input type='text' placeholder='nom' name='nom' />
          <br/>

          <label htmlFor='prenom'> Prenom </label>
          <input type='text' placeholder='prenom' name='prenom' />
          <br/>

          <label htmlFor='entreprise'> Entreprise </label>
          <input type='text' placeholder='entreprise' name='entreprise' />
          <br/>

          <label htmlFor='email'> Email </label>
          <input type='email' placeholder='email' name='email' />
          <br/>

          <label htmlFor='password'> Mot de passe </label>
          <input type='password' placeholder='mot de passe' name='password' />
          <br/>

          <label htmlFor='confirmPassword'> Confirmer le mot de passe </label>
          <input type='password' placeholder='confirmer le mot de passe' name='confirmPassword' />
          <br/>

          <select name='role'>
            <option value='client'>Client</option>
            <option value='jpm'>JPM</option>
            <option value='admin'>Admin</option>
          </select>
          <br/>
          <input type='button' value="Ajouter l'utilisateur" onClick={this.createUser.bind(this)} />
        </form>
        
        
	</div>

    )
  }
}