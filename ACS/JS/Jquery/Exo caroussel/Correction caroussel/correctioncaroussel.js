$(document).ready(function() {
	
	var $car = $("ul.carrousel"); // on va beaucoup réutiliser ce sélecteur

	// on enveloppe l'ul.carrousel dans un main, qui contiendra les boutons
	// et on ajoute une classe slide aux enfants de l'ul.carrousel
	$car.wrap('<main class="carrousel-holder"></main>').children("li").addClass("slide");

	// ce sélecteur sera réutilisé aussi
	var $hld = $("main.carrousel-holder");

	// la section contenant les boutons est insérée après l'ul (append)
	// ainsi, quand on la superposera au carrousel, elle sera sur le dessus
	$hld.append('<section class="controls"><button type="button" id="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button><button type="button" id="prev"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></button></section>');
	// on padde-top la section des boutons de la moitié de la hauteur de la première image
	// le box-sizing de tous les éléments est modifié en border-box par Bootstrap
	// l'ajout du padding retranchera autant de hauteur de contenu de façon à ce que contenu+padding fasse toujours 100% de hauteur
	$hld.children("section.controls").css("padding-top", $("ul.carrousel > li:first-child").height() / 2);

	// début des choses sérieuses

	// permet de contrôler si une animation est en cours
	var animated = false;
	$("ul.carrousel").on("next.carrousel", function () {
		if (animated) { // si en cours d'animation, on quitte sans rien faire
			return;
		} else {
			animated = true; // on la passe à true, les autres animations seront bloquées
			// le premier li est décalé vers la gauche d'une largeur d'image
			$(this).children(":first-child").animate({marginLeft: "-100%"}, function() {
				// à la fin de l'animation, cette même li est déplacée après la dernière li actuelle et sa marge est remise à zéro (sans animation)
				$(this).insertAfter($(this).siblings(":last-child")).css({marginLeft: 0});
				// on débloque animated, le prochain event prev ou next relancera une animation
				animated = false;
			});
		}
	}).on("prev.carrousel", function() {
		if (animated) return; // syntaxe plus courte, très répandue
		// pas de else, car si on arrive ici c'est que animated est false : si animated était true, la fonction se serait arrêtée à la ligne précédente
		animated = true; // idem next
		// la dernière li est insérée avant la première, on lui applique immédiatement une marge pour continuer de voir l'ancienne première li dans le carrousel
		$(this).children(":last-child").insertBefore($(this).children(":first-child")).css({marginLeft: "-100%"});
		// on anime cette dernière li devenue première en remettant progressivement sa marge à 0
		$(this).children(":first-child").animate({marginLeft: 0}, function() {
			// à la fin de l'animation, on débloque animated
			animated = false;
		});
	});

	// boutons
	// facile : à l'appui sur un bouton, on déclenche l'évènement correspondant
	$("button#next").on("click", function() {
		$car.trigger("next.carrousel");
	});

	$("button#prev").on("click", function() {
		$car.trigger("prev.carrousel");
	});

	// autoplay
	// un peu moins facile : l'animation automatique quand la souris n'est pas sur le carrousel
	var autoPlay; // contiendra l'id de l'interval créé, ou 0 si pas d'interval
	$hld.on("mouseenter", function() {
		// quand on rentre dans le carrousel, on supprime l'interval grâce à son id
		clearInterval(autoPlay);
		// puis on le met à zéro (ajout nécessaire pour faire fonctionner les boutons)
		autoPlay = 0;
	}).on("mouseleave", function() {
		// quand on quitte le carrousel, c'est reparti, on stocke l'id du nouvel interval dans autoPlay
		autoPlay = setInterval(function() {
			$(".carrousel").trigger("next.carrousel");
		}, 1500);
	}).trigger("mouseleave"); // on déclenche manuellement un mouseleave pour lancer un interval
	// si au chargement, la souris est hors du carrousel, tout se passe bien
	// si elle est dans le carrousel, le carrousel sera animé jusqu'à ce que la souris bouge, même d'un pixel

	// touches
	// bonne pratique pour la lisibilité : créer des constantes (traditionnellement écrites en majuscules)
	// ici, une par touche pour lesquelles on souhaite lancer une action
	var LEFT_KEY = 37;
	var RIGHT_KEY = 39;
	$(document).on("keydown", function(e) {
		switch(e.which) { // e.which contient le key code de la touche appuyée
			case LEFT_KEY:
				// touche gauche, slide précédent
				$car.trigger("prev.carrousel");
				break;
			case RIGHT_KEY:
				// touche droite, slide suivant
				$car.trigger("next.carrousel");
				break;
			default:
				// pour toutes les autres touches, on termine la fonction, pour éviter de relancer l'interval pour rien
				return;
		}
		// on atteint donc cette ligne uniquement si on a appuyé sur gauche ou droite
		if (autoPlay) { // rappelons qu'autoPlay contient soit un id non nul (si interval en cours) soit 0 (si pas d'interval)
			// donc si interval en cours, on l'arrête
			clearInterval(autoPlay);
			// et on en relance un nouveau en déclenchant manuellement un mouseleave
			$hld.trigger("mouseleave");
		}
	});

	// drag
	// la partie légèrement plus compliquée
	$hld.on("mousedown", function(e) {
		// on retient la coordonnée x de l'appui de souris
		var baseX = e.offsetX;
		// pour faire zoli, on ajouter une classe au main, ce qui change le curseur (préconfiguré dans le css)
		$(this).addClass("grabbing");
		// on branche un écouteur de mouvement
		$(this).on("mousemove", function(e) {
			// si on s'est déplacé vers la droite, l'offset actuel est plus grand que celui au clic
			if (e.offsetX - baseX > 150) {
				// si cette différence dépasse 150px, on déclenche un prev
				$car.trigger("prev.carrousel");
				// et on définit l'offset actuel comme la nouvelle base, ce qui permet de continuer d'animer si on continue de bouger la souris
				baseX = e.offsetX;
			// si on s'est déplacé vers la droite, e.offsetX est plus petit que baseX 
			} else if (e.offsetX - baseX < -150) {
				$car.trigger("next.carrousel");
				baseX = e.offsetX;
			}
		});
	}).on("mouseup", function() { // quand on relâche le bouton de la souris
		// on retire l'écouteur de mouvement, pour ne plus animer quand on bouge la souris horizontalement
		$(this).off("mousemove");
		// et on retire la classe du main, le curseur redevient donc celui par défaut
		$(this).removeClass("grabbing");
	});

	// TADA !
});