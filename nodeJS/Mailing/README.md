# Expression fonctionnelle du besoin : 
un outil à disposition de l’entreprise pour les salariés qui souhaitent envoyer des cartes de voeux personnalisées (à leurs clients et/ou à leurs collaborateurs).

## Contraintes :

* filtrer les utilisateurs,
* modification d’une partie de l’email,
* limiter le nombre d’envoi d’email.
* envoyer le token pour validation
* afficher la campagne correspondante pour édition (carte + champs textes) si le token est valide
* afficher une page d'erreur si le token n'est pas valide
* rediriger vers la previsualisation (on valide et on lance alors la campagne, on ne valide pas et on retourne alors à l'édition)
* lancer la campagne, on envoie la carte personnalisée à une liste de destinataires (renseignée manuellement ou importée d'un csv)

# Outils :

## Les solutions proposées pour répondre au besoin.

Création d'une application web qui va permettre une campagne d'emailing en phase avec la politique de communication de l'entreprise.

Le projet est partagé en 2 projets distincts qui communiquent entre elles :
- [Back-office en NodeJS]()
- [Front-office en React]()
  - [ReactJS](https://facebook.github.io/react/)
  - [Material-UI](http://www.material-ui.com/)
