<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lipsum extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->view('common/header');
		$this->load->view('paragraphe', $data); // pour le moment il est vide
		$this->load->view('common/footer');
	}

	public function genererParagraphesController ($nbParagraphe, $nbMot) {

		$nbParagraphe = $this->input->get("nbParagraphe");
		$nbMot =  $this->input->get("nbMot");

		/*$this->load->model("Lorem_model");*/

		$data["paras"] = $this->Lorem_model->genererParagraphesModel($nbParagraphe, $nbMot);


		$this->load->view('common/header');
		$this->load->view('paragraphes', $data); // pour le moment il est vide
		$this->load->view('common/footer');
	} 

}
