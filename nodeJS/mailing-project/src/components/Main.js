import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Layout from './Layout';
import NotFound from './NotFound';

injectTapEventPlugin();  // Evite le "Warning: Unknown prop `onTouchTap` on <span> tag."

class Main extends React.Component {
	constructor() {
		super();

		this.state = {
			display: ""
		};
  	}

  	componentWillMount() { //Launch before the component mount

  		fetch('http://faridl.dijon.codeur.online:8080/token/verif?token='+this.props.match.params.token) //sending the token we got from the url
  		.then(res => {
  			return res.json();
  		})
		.then(res => {
			if (!res.isLoggedIn) {
				this.setState({display: <NotFound />});
			} else {
				this.setState({
					display: <Layout gabarit={res.gabarit} campagne={res.nom_campagne} />  //We call Layout and we pass datas to it
				})
			}
		})
		.catch(err => {
			console.log(err);
			this.setState({display: <NotFound />});
		});
  	}

	render() {
	    
	    return (
	    	<div>
		    	<div className="layout">
		    		{this.state.display} 
		        </div>
		    </div>
	    );
	}
}

export default Main;