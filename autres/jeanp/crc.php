<!DOCTYPE html>
<html>
<head>
	<title>Exos crypto / CRC</title>
</head>
<body>
	<?php require 'menu.php'; ?>
	<p>Tout se passe dans la console :</p>
	<ul>
		<li>crc(<var>message</var>) : retourne le crc du <var>message</var> (8 caractères max)</li>
		<li>checkcrc(<var>message</var>, <var>crc</var>) : retourne vrai si le <var>crc</var> correspond au <var>message</var></li>
		<li>repaircrc(<var>message</var>, <var>crc</var>) : retourne le <var>message</var> réparé</li>
	</ul>
	<script type="text/javascript">

		function crc(message) {

			// on ne gère pas le cas des messages supérieurs à 8 caractères pour l'instant
			if (message.length > 8) {
				console.log("Cette fonction n'est prévue que pour des messages de 8 caractères maximum");
				return;
			}

			// si le message est trop court, on le remplit avec des octets 0 (pas des caractères "0", dont la valeur ASCII est 48 !)
			while (message.length < 8) {
				message += String.fromCharCode(0);
			}

			// on crée le tableau d'octets, formatés en chaînes de 8 bits
			var octets = [];
			for (var i = 0; i < message.length; i++) {
				// message(string).charCodeAt=>(int).toString=>(string "binaire").slice=>(8 derniers bits de la string "binaire") 
				octets.push(("0000000" + message.charCodeAt(i).toString(2)).slice(-8));
			}

			// bits => les bits, ones => les 1 de la colonne en cours
			var bits = ["",""], ones;

			// caractère crc 1
			// parcours "colonne par colonne"
			for (var i = 0; i < octets[0].length; i++) {
				// on remet ones à 0 à chaque tour
				ones = 0;
				// parcours les bits de la colonne en cours
				for (var j = 0; j < octets.length; j++) {
					// le bit 0 de l'octet 0, puis le bit 0 de l'octet 1, le bit 0 de l'octet 2 etc...
					if (octets[j].charAt(i) == "1") { // si c'est un 1, on incrémente ones
						ones++;
					}
				}
				// après avoir compté les 8 bits, on modulo 2 et on ajoute le résultat à la string du premier bit
				bits[0] = bits[0] + ones % 2;
			}

			// ici, bits contient ["xxxxxxxx", ""] çàd que le premier bit est calculé

			// caractère crc 2
			// parcours "ligne par ligne"
			for (var i = 0; i < octets.length; i++) {
				ones = 0;
				for (var j = 0; j < octets[i].length; j++) {
					// le bit 0 de l'octet 0, puis le bit 1 de l'octet 0, le bit 2 de l'octet 0 etc...
					if (octets[i].charAt(j) == "1") {
						ones++;
					}
				}
				bits[1] = bits[1] + ones % 2;
			}

			return String.fromCharCode(parseInt(bits[0], 2), parseInt(bits[1], 2));
		}

		function checkcrc(message, controle) {
			return crc(message) == controle;
		}

		function repaircrc(message, controle) {
			if (checkcrc(message, controle)) return message;
			var controleFoireux = crc(message);
			var coordsX = controleFoireux.charCodeAt(0) ^ controle.charCodeAt(0);
			var coordsY = controleFoireux.charCodeAt(1) ^ controle.charCodeAt(1);
			var iErreur = Math.log2(coordsY);
			var jErreur = Math.log2(coordsX);
			if (jErreur != Math.floor(jErreur) && iErreur != Math.floor(iErreur) || coordsX == 0 || coordsY == 0) {
				console.log("Erreurs non corrigibles avec le CRC");
				return message;
			}
			while (iErreur > 0) {
				message = message.substr(0, 7 - iErreur) + String.fromCharCode(message.charCodeAt(7 - iErreur) ^ coordsX) + message.substr(8 - iErreur);
				iErreur = coordsY - Math.pow(2, Math.floor(Math.log2(coordsY)));
			}
			return message;
		}

	</script>
</body>
</html>