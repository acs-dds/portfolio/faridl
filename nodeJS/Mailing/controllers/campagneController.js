var campagne = require('../app/models').campagne;

exports.create = function (req, res, err) {
	// if(!(
	// 	req
	// 	&& req.body
	// 	&& req.user
	// 	)
	// ){
	// 	var message;
	// 	if(!req.user){
	// 		return res.json(new Error("Vous n'êtes pas connecté"), 403);
	// 	} else {
	// 		return res.json(new Error("Paramètres non définis"), 403);
	// 	}
	// }

	console.log(req.headers.nom_campagne);
	req.assert('nom_campagne', 'Veuillez insérer le nom de la campagne').notEmpty();
	


	const validationErrors = req.validationErrors();
	console.log(validationErrors);

	if (validationErrors) {
		//console.log(validationErrors);
		return res.json(validationErrors);
	}

	var params = req.body;
	console.log(params);
	params.userId = req.user.id;

	return campagne.create(params)
	.then(res.json)
	.catch(res.json(err));
};


// les données sont envoyées en REST par REACT nom campagne
