<?php

// je prévois de faire quelques tests d'affichage simples, donc je spécifie un type texte plat, ainsi je n'aurai pas besoin d'écrire du html pour avoir un affichage correct de mes sauts de ligne, par exemple
header("Content-Type: text/plain");


require __DIR__.'/../daily/2501.php';
// le fichier ne contient qu'une function encode qui prend un paramètre nommé $character
// deux nouvelles fonctions sont utilisées : chr() et ord()
// 3 minutes de RTFM plus tard, j'ai compris que ord() prend un caractère en argument et retourne le code ASCII du caractère et chr() fait l'inverse, il retourne le caractère correspondant au code ASCII passé en argument
// je prends 2-3 minutes pour faire des tests et vérifier que j'ai bien compris
echo ord('a').PHP_EOL;
echo ord('-').PHP_EOL;
echo ord('@').PHP_EOL;
echo chr(68).PHP_EOL;
echo chr(54).PHP_EOL;
echo chr(219).PHP_EOL;
// ligne 4 : $c contient donc le code ASCII du caractère passé en argument
// ligne 5 : $c % 3 (le modulo correspond au reste de la division, par exemple 17 divisé par 3 font 5, reste 2, donc 17 % 3 = 2)
// si le code ASCII est multiple de 3 (donc si $c % 3 est égal à 0), 0 sera considéré (casté) comme false, donc l'inverse (!) de false étant true, on ira dans le if
// si le code n'est pas multiple de 3, 1 ou 2 (ou plus généralement n'importe quel entier) sera casté comme true, donc !true = false => on ira dans le else
// ligne 7 : le else est tout simple, il retourne le code ASCII reconverti en caractère, qui sera donc inchangé
// ligne 6 : le if est légèrement plus complexe, le code ASCII est modifié, il est multiplié par 2 puis on lui ajoute 15 avant de reconvertir le résultat en caractère, qui sera donc changé

// venons en à l'exercice : utiliser la fonction pour produire un résultat quelconque qui prouverait que j'ai compris la fonction
// puisqu'elle encode un caractère, je vais simplement boucler sur une chaîne de caractère et appeler cette fonction sur chaque caractère, il en résultera une chaîne de caractère "encodée"
$maChaine = "Testons donc cette fonction encode";

for ($i=0; $i < strlen($maChaine); $i++) { // je boucle en utilisant la longueur de ma chaîne comme limite
	echo encode($maChaine[$i]); // PHP permet de passer un indice à une chaîne, ce qui permet d'accéder au caractère à cet indice, il n'y a plus qu'à l'encoder et afficher le résultat
}