
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var db = require('../app/models');


exports.create = function (req, res, err) {

	if(!req.body){
		var message;
		message = "un problème s'est produit avec le formulaire";
	}
	return 

	var nom = req.body.nom;
	var prenom = req.body.prenom;
	var email = req.body.email;
	var password =req.body.password;
	var confirmPassword = req.body.password2;


	req.assert('email', 'Email au mauvais format').isEmail();
	req.assert('password', 'Veuillez insérer un mdp').notEmpty();
	req.sanitize('email').normalizeEmail({ remove_dots: false });
	req.assert('email', 'Votre adresse email n\'est pas valide').isEmail();
	req.assert('password', 'le mdp est trop court').len(3);
	req.assert('confirmPassword', 'les mdp sont différents').equals(req.body.password);

	const validationErrors = req.validationErrors();

	if (validationErrors) {
		//console.log(validationErrors);
		res.render('register', {
			errors : validationErrors
		});
	}

	return db.user.create({
		nom : nom,
		prenom : prenom,
		email : email,
		password : password
	})
	.then(function(user){
		res.redirect('/login');
	})
	.catch(function(err){
        console.log(err);
		res.render('register', {
			messages : err.errors
		});
	});
};

