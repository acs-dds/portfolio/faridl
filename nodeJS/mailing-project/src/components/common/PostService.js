class PostService {

	sendData(url, data) {
		return fetch(url, {
			method: 'POST',
			credentials: 'include',
			headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: data
		})
		.then(res => {
			return res.json();
		})
		.catch(err => {
			console.log(err);
		})
	}
}

export default new PostService();