import React from 'react';
import Dropzone from 'react-dropzone';

export default class Contact extends React.Component {

	render() {
		let list = [];
		let sendButton = null;

		for(let i = 0; i < this.props.infos.length; i++) {
			list.push(<label key={i}>
								<input type="checkbox" id={"checked"+i} />
								<input type="hidden" id={"emailAddress"+i} value={this.props.infos.slice(i,i+1)[0].emailAddress} />
								<input type="hidden" id={"firstName"+i} value={this.props.infos.slice(i,i+1)[0].firstName} />
								<input type="hidden" id={"lastName"+i} value={this.props.infos.slice(i,i+1)[0].lastName} />
									{this.props.infos.slice(i,i+1)[0].lastName + ' ' + this.props.infos.slice(i,i+1)[0].firstName + ' - ' + this.props.infos.slice(i,i+1)[0].emailAddress}
							<br /></label>);
		}

		if(list.length > 0) {
					sendButton = <button
						type="button"
						onClick={this.props.sendInfos}
					>
						Send
					</button>
				}

		return (
			<div className="div-contact">
				<div className="drop-div">
					<Dropzone
						accept="text/csv"
						onDrop={this.props.droppedFile}
					>
						Drop a csv file here...
					</Dropzone>
					{sendButton}
				</div>
				<div>
					{list}
					{this.props.successMsg}
				</div>
			</div>
		);
	}
}