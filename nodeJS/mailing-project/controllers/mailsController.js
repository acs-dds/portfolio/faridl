const nodemailer = require("nodemailer");
const EmailTemplate = require('email-templates').EmailTemplate;
const path = require('path');
const campagneController = require('./campagneController');
const Promise = require("bluebird");
const _ = require("lodash");

const templatesDir = path.resolve(__dirname, '..', 'templates');
const template = new EmailTemplate(templatesDir);

const transporter = nodemailer.createTransport({
	service: "gmail",
	auth: {
		user: process.env.GMAIL_USER,
		pass: process.env.GMAIL_PASS
	}
});

exports.sendMails = function (req, res, err) {

	console.log('===========< sendMails');

	var publiListOkPromise;
	if(!(req && req.body && req.body.publiList)) {
		publiListOkPromise = Promise.reject(new Error("aucun contact n'a été trouvé"));
	} else {
		publiListOkPromise = Promise.resolve();
	}
	// TODO : valider la campagne
	// TODO : valider le quota de la campagne p|r à la longueur du tableau de publi
	// TODO : valider req.body.exp
	// TODO : valider req.body.obj (longueur)
	
	var sentEmailsCount = 0;
	var sendErrors = [];
	var sendResults = [];
	var message = "Envoi des e-mails en cours...";
	Promise.all([
		publiListOkPromise/*,
		checkCampaign(req.body.camp),
		checkCampaignQuota(req.body.camp, req.body.publiList.length)*/
	])
	.then(function(){
		// allGood
		console.log("allOk", req.body.publiList);
		var localsArray = _.map(req.body.publiList, function transformToLocals(publiItem){
			return _.merge({
			  	message: req.body.msg,
			  	campagne: req.body.camp,
			  	image: 'cid:zombie1@jpm-next.com'
			}, publiItem);
		});
		return localsArray;
	})
	//ne passe pas par là
	.map(function(locals){
		console.log("render mapped locals", locals);
		return new Promise(function(resolve, reject){
			template.render(locals, function(err, result){
				if(err){
					return reject(err);
				}
				resolve(result);
			});
		})
		.then(function(rendered){
			console.log("rendered mapped locals", locals);
			return {
				rendered: rendered,
				locals: locals
			};
		})
	})
	.then(function(result){
		console.log("rendered all mapped locals", result);
		return result;
	})
	.map(function(resultWithRenderedAndLocals){
		console.log("resultWithRenderedAndLocals", resultWithRenderedAndLocals);

		return new Promise(function(resolve, reject){
			transporter.sendMail({
				from: req.body.exp,
				to: resultWithRenderedAndLocals.locals.emailAddress,
				subject: req.body.obj,
				html: resultWithRenderedAndLocals.rendered.html,
				attachments:[{
					filename: req.body.img,
					path: path.join(__dirname, '..', 'src/data/img/' + req.body.img),
					cid: 'zombie1@jpm-next.com'
	            }]
			}, function(err, result){
				if(err){
					return reject(err);
				}
				resolve(result);
			});
		})
		.then(function(result){
			console.log("Mail envoyé avec succès!", result);
			sentEmailsCount++;
			sendResults.push(result);
			return result;
		})
		.catch(function(err){
			console.error("Erreur lors de l'envoie du mail!", err);
			sendErrors.push(err);
			return;
		})
		.finally(function(){
			return transporter.close();
		});
	}, {
		concurrency: 10
	})
	.then(function(){
		console.log("updateQuota", sentEmailsCount);
		return campagneController.updateQuota(sentEmailsCount, req.body.camp);
	})
	.then(function formatResults(){
		console.log("formatResults", sendResults, sendErrors);
		message = `${sendResults.length} messages sont partis`;
		if(sendErrors.length){
			message += `"\r\n${sendErrors.length} erreurs se sont faux filet !`;
		}
		return {
			sendErrors,
			sendResults,
			message
		}
	})
	.then(res.json.bind(res))
	.catch(res.json.bind(res));
}

