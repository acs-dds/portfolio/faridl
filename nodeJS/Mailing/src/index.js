import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Routes from './Routes';

import './data/css/index.css';

const App = () => (
  <MuiThemeProvider>
    <Routes />
  </MuiThemeProvider>
);

ReactDOM.render(
<App />,
  document.getElementById('root')
);