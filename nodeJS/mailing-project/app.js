//library
require('dotenv').config();
const pg = require('pg'); 
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const jwt = require('jsonwebtoken');
const flash = require('connect-flash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const acl = require('./config/acl');

const expressValidator = require('express-validator');
const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();

console.log('je suis dans le bon dossier');

// view engine setup
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine', 'pug');

//using settings
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));


//Express Session
app.use(session({
	cookie: { maxAge: 6000000 }, // configure when sessions expires 
	saveUninitialized: true, 
	resave: false, // do not automatically write to the session store
	secret: process.env.SECRET_KEY
}));

//Passport init
app.use(passport.initialize());
app.use(passport.session());
require ('./config/passport.js')(passport);

//Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', process.env.ORIGIN);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, application/x-www-form-urlencoded, Authorization, X-Requested-With');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

//Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
	var namespace = param.split('.')
	, root    = namespace.shift()
	, formParam = root;

	while(namespace.length) {
	  formParam += '[' + namespace.shift() + ']';
	}
	return {
	  param : formParam,
	  msg   : msg,
	  value : value
	};
  }
}));

// hash password de passport
SALT_WORK_FACTOR=process.env.SALT_WORK_FACTOR;

//Connect Flash
// app.use(flash());

// app.use(function(req, res, next) {
//   res.locals.messages = req.flash();
//   next();
// });

// Controllers (route handlers).
const connexionController = require('./controllers/connexionController');
const tokenController = require ('./controllers/tokenController');
const userController = require ('./controllers/userController');

const entrepriseController = require('./controllers/entrepriseController');
const campagneController = require('./controllers/campagneController');

//Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));
const mailsController = require('./controllers/mailsController');

//token

app.get('/token', tokenController.getToken);
app.get('/token/verif', tokenController.verifyToken);

//authentification
 app.post('/authenticate', passport.authenticate('local'), function(req, res) {
 	res.json({ sessionID: req.sessionID });
 });


//routes with authentification and authorization middlewares
app.get('/logout', connexionController.destroySession);

app.post('/mail', mailsController.sendMails);

app.post('/user', [connexionController.IsAuthenticated, acl.middleware()], userController.create);

app.post('/entreprise', [connexionController.IsAuthenticated, acl.middleware()], entrepriseController.create);

app.post('/campagne', [connexionController.IsAuthenticated, acl.middleware()], campagneController.create);
app.get('/campagne', [connexionController.IsAuthenticated, acl.middleware()], campagneController.get);

app.delete('/campagne', campagneController.delete);

app.put('/campagne', [connexionController.IsAuthenticated, acl.middleware()], campagneController.update);

//Models
var models = require("./app/models");
 
//Sync Database
models.sequelize.sync().then(function() {
	console.log('connecté à la BDD');
}).catch(function(err) {
	console.log(err, "un problème avec la connexion pg!");
});


app.listen(process.env.PORT);

module.exports = app;