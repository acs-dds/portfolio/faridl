var entreprise = require('../app/models').entreprise;


exports.create = function (req, res, err) {
	if(!(
		req
		&& req.body
		)
	){
		var message;
		if(!req.body){
			message = "données mal renseignées";
		}
		return res.render('dashboard', {
			errors : [
				new Error(message)
			]
		});
	}

	var nom = req.body.name;

	req.assert('name', 'Veuillez insérer le nom de l\'entreprise').notEmpty();

	const validationErrors = req.validationErrors();

	if (validationErrors) {
		//console.log(validationErrors);
		return res.error(validationErrors);
	}


	return db.entreprise.create({
		nom_entreprise : nom
	})
	.then(function(entreprise){
		console.log('created entreprise', entreprise);
		res.render('dashboard', 
			{message : 'entreprise enregistrée'});
	})
	.catch(function(err){
		console.log(err);
		res.render('dashboard', {
			messages : err.errors
		});
	});
};

