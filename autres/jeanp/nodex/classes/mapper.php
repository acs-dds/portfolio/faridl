<?php

require_once 'client.php';
require_once 'produit.php';

class Mapper {

	public function getClient($login, $mdp) {
		$h = fopen('data/clients.csv', 'r');

		while ($ligne = fgetcsv($h, 0, ';')) {
			if ($ligne[2] == $login && $ligne[3] == md5("xedon9:{$mdp}_*jura")) return new Client($ligne);
		}
		return false;
	}

	public function getProduit($ref) {
		$h = fopen('data/produits.csv', 'r');

		// lecture du fichier ligne par ligne
		while ($ligne = fgetcsv($h, 0, ';')) {
			if ($ligne[0] == $ref) {
				if (substr($ligne[0], -3) != "lib") {
					return new Produit($ligne);
				} else {
					return new ProduitLibre($ligne);
				}
			}
		}
		return false;
	}

	public function getCatalogue($typo) {
		$cat = [];

		$h = fopen('data/produits.csv', 'r');

		// lecture du fichier ligne par ligne
		while ($ligne = fgetcsv($h, 0, ';')) {
			// on décompose les typologies de client pour chaque produit
			$typos = array_map('trim', explode(',', $ligne[5]));
			// si la typo donnée fait partie des typologies pour le produit en cours

			if (in_array($typo, $typos)) {
				// on ajoute le produit au catalogue
				if (substr($ligne[0], -3) != "lib") {
					$cat[] = new Produit($ligne);
				} else {
					$cat[] = new ProduitLibre($ligne);
				}
			}
		}

		return $cat;
	}
}