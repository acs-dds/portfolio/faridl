<?php 

class LoginMapper extends CI_Model{

	public function __construct(){
	}

	public function inscription_model ($nom, $prenom, $pseudo, $email, $mdp){
		$dataUser = array(
	        'nom' => $nom,
	        'prenom' => $prenom,
	        'pseudo' => $pseudo,
	        'email' => $email,
	        'mdp' => $this-> hash($mdp)
		);

		$this->db->insert('utilisateur', $dataUser);
	}//fin fonction insert

	private function hash ($pass) {
		return md5($pass);
	}//fin de hash


    public function checkUser($login, $mdp) {
        $sql = "SELECT * FROM utilisateur WHERE mdp = ? AND pseudo = ?";

		$checkUser = $this->db->query($sql, array($this->hash($mdp), $login));
		return $checkUser->num_rows();

    }//fin de checkUser

    public function ping_model ($idUtilisateur) {
		$this->db->query("UPDATE utilisateur SET ping = now() WHERE id=?", [$idUtilisateur]);

		return 0;
	}//fin de ping

	public function getUtilisateursEnLigne_model () {
		return $this->db->query("SELECT pseudo FROM utilisateur WHERE ping > now() - '1 day'::interval;") ->result_array();
	}//fin de getUtilisateursEnLigne

//fin MessageMapper
}