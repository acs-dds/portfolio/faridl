<?php require_once "classes/classeClients.php";
require_once "classes/classeArticle.php";
session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" href="favicon.png" />
	<title>NODEX | Bon de commande</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
</head>
<body class="body_catalogue">
<?php var_dump($_POST['wRec']); ?>
<div class="total_paie">
	<div class="recap">
		<center><h2 class="titre_recap">Récapitulatif de la commande</h2></center>
		<table class="liste_recap">
			<tr>
				<td class="colonne">Essence du bois: </td>
				<td class="colonne2"><?php echo $_SESSION["objet"]->getMatiere(); ?></td>
			</tr>
			<tr>
				<td class="colonne">Référence: </td>
				<td class="colonne2"><?php echo $_SESSION["objet"]->getId(); ?></td>
			</tr>
			<tr>
				<td class="colonne">Longueur: </td>
				<td class="colonne2"></td>
			</tr>
			<tr>
				<td class="colonne">Largeur: </td>
				<td class="colonne2"></td>
			</tr>
			<tr>
				<td class="colonne">Epaisseur: </td>
				<td class="colonne2"><?php echo $_SESSION["epaisseur"]; ?></td>
			</tr>
			<tr>
				<td class="colonne">Surface (en m²): </td>
				<td class="colonne2"><!-- Entrer la surface en m² --></td>
			</tr>
			<tr>
				<td class="colonne">Prix (au m²): </td>
				<td class="colonne2"><!-- Entrerle prix au m² --></td>
			</tr>
			<tr>
				<td class="colonne">Gâche (au m²): </td>
				<td class="colonne2"><!-- Entrerle prix au m² --></td>
			</tr>
			<tr>
				<td class="colonne">Total: </td>
				<td class="colonne2"><!-- Entrer le prix total a payer --></td>
			</tr>
		</table>
	</div>

	<section class="cord">
		<center><h2 class="titre_cord">Coordonnées du client</h2></center>
			<ul class="liste_cord">
				<strong><li class="clientcord">Nom : </strong><?php echo $_SESSION["client"]->getNom(); ?></li>
				<strong><li class="clientcord">Prénom : </strong><?php echo $_SESSION["client"]->getPrenom(); ?></li>
				<strong><li class="clientcord">Adresse de livraison : </strong><?php echo $_SESSION["client"]->getVoie(); ?></li>
				<strong><li class="clientcord">Code Postal : </strong><?php echo $_SESSION["client"]->getCodePostal(); ?></li>
				<strong><li class="clientcord">Ville : </strong><?php echo $_SESSION["client"]->getVille(); ?></li>
			</ul>
	</section>
</div>

	<section class="paiement">
		<center><h2 class="titre_moyen">Moyens de paiement</h2></center><br>
		<center><p class="moyen">(PayPal, carte VISA, carte bancaire)</p></center>
		<div class="icons">
			<i class="fa fa-paypal fa-4x" aria-hidden="true"></i>
			<i class="fa fa-cc-visa fa-4x" aria-hidden="true"></i>	
			<i class="fa fa-credit-card-alt fa-4x" aria-hidden="true"></i>
		<div class="icons">

		<button class="bouton_paie" type="submit">Payer</button>	
	</section>

<footer>
	<center><div class="informations_footer">
		<p>NODEX - 14, rue du Général de Gaulle - Z.I. Le Pré vert - 39500 Moirans-en-Montagne<br>
		Tél: 03.80.45.78.66<br>
		Copyright © NODEX-Industrie</p>
	</div></center>
</footer>

</body>