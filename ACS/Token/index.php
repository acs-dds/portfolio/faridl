<?php
require_once 'TokenController.php';
//On démarre les sessions
session_start();

$test = new TokenController();

$time_expiration = time() + (2629800*2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mon formulaire anti CSRF</title>
</head>

<body>
<form id="form" name="form" method="get" action="">
  <p>Pseudo : <label><input type="text" name="pseudo" id="pseudo" /></label></p>
  <p>E-mail : <label><input type="text" name="email" id="email" /></label> </p>
  <p>Nom : <label><input type="text" name="nom" id="nom" /></label></p>
  <p>
       <label for="hotel">Dans quel hotel étiez-vous ?</label><br />
       <select name="hotel" id="hotel">
  		   <option disabled >Choisissez votre hotel</option>
         <option value="Accor">Accor</option>
         <option value="Classe Premiere">Classe Premiere</option>
         <option value="Mercure">Mercure</option>
         <option value="B&B">B&B</option>
       </select>
  </p>
 
  <input type="text" name="token" id="token" size="60" value="<?php echo $test->generer_Token_CI($_GET["nom"],$_GET["hotel"]); ?>"/></p>
  <p><label><input type="submit" name="Envoyer" id="Envoyer" value="Envoyer" /></label></p>
</form>
<?php echo $test->verifier_Token_CI($time_expiration,$_SERVER['HTTP_REFERER'],$_GET["nom"]);?>
</body>
</html>