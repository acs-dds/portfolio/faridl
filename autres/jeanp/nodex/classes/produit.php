<?php

class Produit {
	protected $ref;
	protected $essence;
	protected $longueur;
	protected $largeur;
	protected $epaisseurs;
	protected $prixGache;
	protected $prixForme;

	public function __construct($array) {
		$this->ref = $array[0];
		$this->essence = $array[1];
		$this->longueur = $array[2];
		$this->largeur = $array[3];
		$this->epaisseurs = array_map('trim', explode(',', $array[4]));
		$this->prixGache = $array[6];
		$this->prixForme = $array[7];
	}

	public function getVolume($epaisseur) {
		return $this->longueur * $this->largeur * $epaisseur / 1000;
	}

	public function getLongueur() {
		return $this->longueur;
	}

	public function getLargeur() {
		return $this->largeur;
	}

	public function getPrixGache() {
		return $this->prixGache;
	}

	public function getPrixForme() {
		return $this->prixForme;
	}

	// cette fonction permet d'afficher le produit en html dans la page catalogue
	public function renderHtml($epaisseur = null) {
		$class = "normal";
		$longueur = $this->longueur;
		$largeur = $this->largeur;
		$href = "decouper.php?ref={$this->ref}&ep={$this->epaisseurs[0]}";
		ob_start();
		require "/home/jeanp/nodex/tpl/produit.tpl";
		return ob_get_clean();
	}

	// méthode magique pour "stringifier" l'objet
	// ici, on va simplement utiliser la représentation html
	public function __toString() {
		return $this->renderHtml();
	}
}

class ProduitLibre extends Produit {
	private $increments = 50;

	public function renderHtml($epaisseur = null) {
		$class = "libre";
		$longueur = "<input type=\"number\" name=\"lo\" min=\"{$this->longueur}\" value=\"{$this->longueur}\" step=\"{$this->increments}\">";
		$largeur = "<input type=\"number\" name=\"la\" min=\"{$this->largeur}\" value=\"{$this->largeur}\" step=\"{$this->increments}\">";
		$href = "decouper.php?ref={$this->ref}&ep={$this->epaisseurs[0]}&lo={$this->longueur}&la={$this->largeur}";
		ob_start();
		require "/home/jeanp/nodex/tpl/produit.tpl";
		return ob_get_clean();
	}
}