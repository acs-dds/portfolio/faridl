<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> Farid Profile </title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <script src="assets\jquery\jquery-3.1.1.js"></script>
    <script src="assets\js\bootstrap.min.js"></script>
    <script src="caroussel2.js"></script>
    <script src="assets\js\scripts.js"></script>


  </head>

  <body>

  <main>
    <!-- Static navbar -->
    <?php include "navigation.html"; ?>



	

	<!-- +++++ Welcome Section +++++ -->
	<div id="ww">
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 centered">
					<img src="assets/img/user.png" alt="Farid">
					<h1>Hey mates !</h1>
					<p>Hello everybody. I'm Farid, a free handsome bootstrap theme coded by BlackTie.co. A really simple theme for those wanting to showcase their work with a cute & clean style.</p>
					<p>Please, consider to register to <a href="http://eepurl.com/IcgkX">our newsletter</a> to be updated with our latest themes and freebies. Like always, you can use this theme in any project freely. Share it with your friends.</p>
				
				</div><!-- /col-lg-8 -->
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /ww -->
	
	
	<!-- +++++ Projects Section +++++ -->
	
	<div class="container pt" id="work">
		<div class="row mt centered">	
			<div class="col-lg-4">
				<a class="zoom green" href="mywork.html"><img class="img-responsive size" src="assets/img/portfolio/simon.jpg" alt="" /></a>
				<p>Le jeu du Simon</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="mywork.html"><img class="img-responsive size1" src="assets/img/portfolio/fourchette.png" alt="" /></a>
				<p>Le jeu de la fourchette</p>
			</div>
			<div class="col-lg-4">
				<a class="zoom green" href="mywork.html"><img class="img-responsive size2" src="assets/img/portfolio/calculatrice2.jpg" alt="" /></a>
				<p>Calculatrice</p>
			</div>
		</div><!-- /row -->
		<div class="row mt centered">	
			<div class="col-lg-4">
				<a class="zoom green" href="mywork.html"><img class="img-responsive size3" src="assets/img/portfolio/touche.png" alt="" /></a>
				<p>Home run !</p>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
	
	
	<!-- +++++ sliders +++++ -->
	<section id="slider" class="centered controls">
		<h1>Sliders</h1>
		<button id="prev"> <img src="assets\img\portfolio\flechegauche.jpg" class="fleche" alt="#previous" ></button>
		<button id="next"> <img src="assets\img\portfolio\flechedroite.jpg" class="fleche" alt="#next"></button>
		<ul class = "caroussel">
			<li class="slide"> <img id="slidesimon" src="assets\img\portfolio\simon.jpg" alt=""> </li>
			<li class="slide"> <img id="slidefourchette" src="assets\img\portfolio\fourchette.png" alt=""> </li>
			<li class="slide"> <img id="slidecalculatrice" src="assets\img\portfolio\calculatrice2.jpg" alt=""> </li>
			<li class="slide"> <img id="slidetouche" src="assets\img\portfolio\touche.png" alt=""> </li>
		</ul>

	</section>


	<!-- +++++ Footer Section +++++ -->
	
	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<h4>My place</h4>
					<p>
						+33 6 16 49 69 45, <br/>
						Toronto, Canada.
					</p>
				</div><!-- /col-lg-4 -->
				
				<div class="col-lg-4">
					<h4>My Links</h4>
					<p>
						<a href="#">Dribbble</a><br/>
						<a href="#">Twitter</a><br/>
						<a href="#">Facebook</a>
					</p>
				</div><!-- /col-lg-4 -->
				
				<div class="col-lg-4">
					<h4>About Farid</h4>
					<p>This cute theme was created to showcase your work in a simple way. Use it wisely.</p>
				</div><!-- /col-lg-4 -->
			
			</div>
		
		</div>
	</div>
</main>

<script type="text/javascript">
	
		
</script>

	
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
</body>
</html>
