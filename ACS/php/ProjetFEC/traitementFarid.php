<?php 
  session_start();

  require "/home/jeanp/xdb.php"; 

  $p = array_map("trim", $_POST);
  // récupérer toutes les données de POST
  Xdb::set($p);

  $valide = true;
  $erreurs = [];
  if (strlen($p['nom']) < 3) {
      $valide = false;
      $erreurs[] = 'Le nom n\'est pas assez long';
  }
  if (strlen($p['prenom']) < 3) {
    $valide = false;
    $erreurs[] = 'Le prénom n\'est pas assez long';
  }
  if (preg_match("#^[a-z0-9_.-]+@[a-z0-9-]{2,}\.[a-z]{2,}$#i", $p['email']) === 0) {
    $valide = false;
    $erreurs[] = "L'adresse mail n'a pas un format valide";
  }
  if (strlen($p['message']) < 5 || strpos($p['message'], ' ') === false) {
    $valide = false;
    $erreurs[] = "Le message n'est pas valide, il doit contenir au moins 5 caractères et être composé de plusieurs mots";
  }

//Si on a plusieurs erreurs, seront-elles toutes stockés dans le tableau ?


// valide est faux alors on va dans contact pour afficher les erreurs
  if ($valide == false) {
    $_SESSION['erreurs'] = $erreurs;
    header("Location: http://faridl.dijon.codeur.online/ProjetFEC/contactFarid.php");
    exit;
  }
/*exit;*/
  // si les infos récupérées sont bonnes alors on va dans le portfolio de la personne
  $destinataire = $_POST["destinataire"];
  // $_POST["destinaire"] = on a soit farid soit camille soit edouard
  switch ($destinataire) {
    case "farid":
      header("Location: http://faridl.dijon.codeur.online/ProjetFEC/portofolio.php");
      break;
    case "camille":
      header("Location: http://camillec.dijon.codeur.online/Portfolio/portfolio.php");
      break;
    case "edouard":
      header("Location: http://edouardb.dijon.codeur.online/PHP/portfolio.php");
      break;
  }


?>