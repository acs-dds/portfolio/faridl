<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class ChatController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index() {
		//si la session n'existe pas alors on le redirige vers la page de connexion
		if (!$this->session->userdata('pseudo')){
			redirect('LoginController/index');
		}

		 $this->load->view('tchat');
		 }// fin de index


	public function getMessages ($discussion = 'general'){
		

		if(isset($_POST['discussion'])){
			$discussion = $_POST['discussion'];
		}

		$id_discussion = $this->MessageMapper->getIdDiscussion_Model ($discussion);

		$arrayMessages['messages'] = $this->MessageMapper->getMessages_Model($id_discussion);
		$this->load->view('../views/templates/message_view', $arrayMessages);


		//mettre une valeur à date en session
		 $date = date_create();
		 $date = date_timestamp_set($date,microtime(true));
		 $date = date_format($date,"Y-m-d H:i:s");
		 /*echo $date; //2017-03-09 14:28:05*/
		 //on ajoute à notre session le tableau associatif
		 $this->session->derniereDate = $date;

		 //on actualise le ping quand le user lance la fct getMessage
		 $login = $this->session->userdata('pseudo'); //"farid"
        //on donne une valeur now() à ping  à l'utilisateur
        $this->LoginMapper->ping_model($this->MessageMapper->getIdUtilisateur_Model($login));
		
	}//fin de getMessages

	public function getNewMessages ($discussion ='general', $derniereDate = 0) {
		$derniereDate = $this->session->derniereDate;
		$arrayNewMessages['newMessages']= $this->MessageMapper->getNewMessages_model($discussion, $derniereDate);

		$this->load->view('../views/templates/newMessage_view', $arrayNewMessages);
	}//fin getNewMessages

	public function insertMessage () {

		if(isset($_POST['discussion'])){
			$discussion = $_POST['discussion'];
			$this->MessageMapper->addDiscussion($discussion);
		} else {
			$discussion = 'general';
		}

		if (isset($_POST['message']) && isset($_POST['date']) && isset($discussion) && isset($_POST['pseudo'])) {

			//on test pour récupérer l'id de discussion et id_utilisateur
			$idDiscussion = $this->MessageMapper->getIdDiscussion_Model($discussion);
			$idUtilisateur = $this->MessageMapper->getIdUtilisateur_Model($_POST['pseudo']);

			//on insert les données dans la BDD
			$this->MessageMapper->insertMessage_Model($_POST['message'],$_POST['date'], $idDiscussion, $idUtilisateur);
			
		}//fin du if
	}//fin insertMessage

}//fin ChatController

