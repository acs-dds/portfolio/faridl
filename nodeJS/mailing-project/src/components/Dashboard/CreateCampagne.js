import React from 'react';
import PostService from '../common/PostService';
import serialize from 'form-serialize';

export default class CreateCampagne extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      messages: []
    }

  }
  
  createCampagne() {
    var formData = document.getElementById('form_campagne');
    const data = serialize(formData);

    // We call the server to log the user in.
   PostService.sendData('http://faridl.dijon.codeur.online:8080/campagne', data)
   .then(res => {
    console.log(res);
      this.setState({
        messages : res.messages
      })
   })
   .catch(err => {
      console.log(err);
    })
  }

  render() {

    return (
      <div>
       <div>
           {this.state.messages.map(function(message){
             if (message){
               return (
                 <div key={message.message}>
                   <p>{message.message}</p>
                   <p>{message.msg}</p>
                 </div>
               )
             }
           })}
         </div>
        <form id="form_campagne">
          <label htmlFor='nom_campagne'> Nom campagne </label>
          <input type='text' placeholder='nom' name='nom_campagne' />
          <br/>

          <label htmlFor='debut_campagne'> Date de début de la campagne : </label>
          <input type='date' placeholder='nom' name='debut_campagne' />
          <br/>

          <label htmlFor='fin_campagne'> Date de fin de la campagne : </label>
          <input type='date' placeholder='nom' name='fin_campagne' />
          <br/>

          <label htmlFor='gabarit'> Gabarit </label>
          <input type='url' placeholder='nom' name='gabarit' />
          <br/>

          <label htmlFor='quota'> Nombre d'emails: </label>
          <input type='text' placeholder='nom' name='quota' />
          <br/>

          <input type='button' value="Ajouter une campagne" onClick={this.createCampagne.bind(this)} />
        </form>
	</div>

    )
  }
}


     