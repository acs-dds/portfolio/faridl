var exports = module.exports = {};


exports.register = function (req, res) {
    res.render('register');
}

exports.login = function (req, res) {
	
	res.render('login', {message: req.flash('error')});
}


exports.dashboard = function(req, res) {
    res.render('dashboard');
}

exports.IsAuthenticated = function(req, res, next){
	if(req.isAuthenticated()){
		next();
	} else {
		res.redirect(401,'login');
	}
}

exports.destroySession = function(req, res, next) {
	req.logOut();
	req.session.destroy();
	res.redirect("/login");
}