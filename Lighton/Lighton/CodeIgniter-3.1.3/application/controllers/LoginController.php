<?php
 
class LoginController extends CI_Controller{

    
    public function index(){
        $this->load->view('index');
    }

    public function connexion () {
        
        $login = $this->input->post('login');
        $mdp = $this->input->post('mdp');
    
        //lancer la methode checkuser
        if ($this->LoginMapper->checkUser($login,$mdp) == TRUE) {

            //On met une session login dans un tableau
            $cu = array(
                'pseudo'  => $login
            );
            //on ajoute à notre session le tableau associatif
            $this->session->set_userdata($cu);

            //l'utilisateur rentre la bonne combinaison login mdp donc il va être redirigé au tchat avec sa session cu
            redirect('ChatController/index');
        } else {
            //mauvaise combinaison donc il reste à la connexion
            $this->load->view('index');
        }
    }//fin de connection


    public function inscription () {
        // on lance la fonction si cmdp et cmd sont égales 
        if ($_POST['mdp'] == $_POST['cmdp']) {
            if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['pseudo']) && isset($_POST['email']) && isset($_POST['mdp'])) {
                //j insere les données dans la table utilisateur
                $this->LoginMapper->inscription_model($_POST['nom'],$_POST['prenom'],$_POST['pseudo'],$_POST['email'],$_POST['mdp']);
                //j'informe l'utilisateur qu'il est bien inscrit
                $this->load->view('inscrit');
            }//fin de condition de remplissage données users 
        } else {
             $incorrecte['test'] = "vous avez inséré des mots de passe différents";
             $this->load->view('index', $incorrecte );
        }
       
    }//fin de inscription

   
    public function getUtilisateursEnLigne() {
        //on met dans une session un tableau de l'ensemble des utilisateurs
        $data['users'] = $this->LoginMapper->getUtilisateursEnLigne_model();
        $this->load->view('../views/templates/connected_view', $data);
    }// fin de getUtilisateursEnLigne

    
    public function deconnexion () {
    	$cuDeconnexion = array(
    		'pseudo'
    		);
		$this->session->unset_userdata($cuDeconnexion);
		$this->load->view('index', $cuDeconnexion);
    }//fin deconnexion


 }