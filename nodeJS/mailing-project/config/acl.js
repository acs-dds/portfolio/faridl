const Acl = require('acl');
const AclSeq = require('acl-sequelize');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(process.env.CONNECT_DB);

const acl = new Acl(new AclSeq(sequelize, { prefix: 'acl_' }));

// Roles
acl.allow([
    {
        roles: 'admin',
        allows: [
            { resources: ['/campagne','/user'], permissions: '*' }
        ]
    }, {
        roles: 'jpm',
        allows: [
            { resources: '/user', permissions: '*' },
            { resources: '/campagne', permissions: ['get','post'] },
        ]
    }, {
        roles: 'client',
        allows: [
        	{ resources: '/campagne', permissions: 'get'}
        ]
    }
]);

module.exports = acl;
