import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router';

import forms from './components/Forms';
import Main from "./components/Main";
import Site from "./components/Site";

const Routes = () => (
  <BrowserRouter>
  	<div>
  		<Route exact path="/campagne/:id/:token" component={ Main } />
  		<Route exact path="/" component={ Site } />
  		<Route exact path="/dashboard" component= { forms }/>
	</div>
  </BrowserRouter>
);

export default Routes;